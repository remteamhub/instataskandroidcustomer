package com.yourappsgeek.taxidriv.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 12/10/2018.
 */

public class ConfirmServiceBSFragment extends BottomSheetDialogFragment
{
    TextView price,lines,userName,location;
    SharedPreferences preferences;
    String id,lat,lng;

    public ConfirmServiceBSFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.confirm_service_provider_fragment, container, false);
        preferences=getActivity().getSharedPreferences("Lines",MODE_PRIVATE);
        String name= Constrants.user_name;
        final String id1=SaveSharedPreference.getUser_ID(getContext());
        Log.e("name",name);
        price=v.findViewById(R.id.textView30);
        lines=v.findViewById(R.id.textView27);
        userName=v.findViewById(R.id.textView24);
        location=v.findViewById(R.id.textView22);
        final String address=location.getText().toString();
        userName.setText(name);
        price.setText("$"+preferences.getString("price","price not found"));
        final String service_price=preferences.getString("price","price not found");
        lines.setText(preferences.getString("title","line not found"));
        final String title=lines.getText().toString();
        id=preferences.getString("id","line not found");
        v.findViewById(R.id.btn_go).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
              //  jobcreate(title,service_price,lat,lng,address,id1);
            }
        });
        return v;
    }

    public void showBottomSheetDialogFragment()
    {
        DeliverServiceBSFragment bottomSheetFragment = new DeliverServiceBSFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        this.dismiss();

    }

    void jobcreate(String title, final String service_price, double lat, double lng, final String location_address, final String customer_id) {


        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call =service.jobcreate(title, service_price, lat,lng,location_address,customer_id);

        //calling the api

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
//                        JSONObject data = json.getJSONObject("data");
                            showBottomSheetDialogFragment();
                        }
                    }else {

                        Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

    }

}