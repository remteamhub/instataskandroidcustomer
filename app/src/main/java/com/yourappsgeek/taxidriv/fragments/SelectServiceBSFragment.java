package com.yourappsgeek.taxidriv.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.activities.Payment_Method;

import static android.content.Context.MODE_PRIVATE;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 12/10/2018.
 */

public class SelectServiceBSFragment extends Fragment
{

    CheckBox cbLane1, cbLane2, cbLane3;
    SharedPreferences preferences;
    String id,title,price;
    String id1,title1,price1;
    String id2,title2,price2;
    SharedPreferences.Editor editor;
    boolean check=false;
    TextView locationAddress;
    private BottomSheetBehavior mBehavior;
    public SelectServiceBSFragment()
    {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.select_service_provider_fragment, container, false);

        cbLane1 = v.findViewById(R.id.cbLane1);
        cbLane2 = v.findViewById(R.id.cbLane2);
        cbLane3 = v.findViewById(R.id.cbLane3);
        locationAddress = v.findViewById(R.id.location);

//        if (!location.equals("")){
//            locationAddress.setVisibility(View.VISIBLE);
//            locationAddress.setText(location);
//
//        }else {
//            locationAddress.setVisibility(View.INVISIBLE);
//        }

        cbLane1.setOnCheckedChangeListener(checkedChangeListener);
        cbLane2.setOnCheckedChangeListener(checkedChangeListener);
        cbLane3.setOnCheckedChangeListener(checkedChangeListener);

        preferences=getActivity().getSharedPreferences("Lines",MODE_PRIVATE);
        editor=preferences.edit();
         id= preferences.getString("id0","");
         title= preferences.getString("title0","");
         price= preferences.getString("price0","");

        id1= preferences.getString("id1","");
        title1= preferences.getString("title1","");
        price1= preferences.getString("price1","");

        id2= preferences.getString("id2","");
        title2= preferences.getString("title2","");
        price2= preferences.getString("price2","");

        v.findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (check) {
                    showBottomSheetDialogFragment();

                }else {
                    Toast.makeText(getContext(),"Please Select Line!",Toast.LENGTH_LONG).show();
                }
            }
        });

        v.findViewById(R.id.tvPaymentSelect).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getContext(), Payment_Method.class));
            }
        });
        return v;
    }

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b)
        {
            switch (compoundButton.getId())
            {
                case R.id.cbLane1:
                {
                    if (b)
                    {
                        check=true;
                        cbLane2.setChecked(false);
                        cbLane3.setChecked(false);
                        editor.putString("id",id).commit();
                        editor.putString("title",title).commit();
                        editor.putString("price",price).commit();

                    }
                    break;
                }
                case R.id.cbLane2:
                {

                    if (b)
                    {
                        check=true;
                        cbLane1.setChecked(false);
                        cbLane3.setChecked(false);

                        editor.putString("id",id1).commit();
                        editor.putString("title",title1).commit();
                        editor.putString("price",price1).commit();

                    }
                    break;
                }
                case R.id.cbLane3:
                {
                    if (b)
                    {
                        check=true;
                        cbLane2.setChecked(false);
                        cbLane1.setChecked(false);
                        editor.putString("id",id2).commit();
                        editor.putString("title",title2).commit();
                        editor.putString("price",price2).commit();
                    }
                    break;
                }
            }

        }
    };

    public void showBottomSheetDialogFragment()
    {
        ConfirmServiceBSFragment bottomSheetFragment = new ConfirmServiceBSFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
    }


}