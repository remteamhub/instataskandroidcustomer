package com.yourappsgeek.taxidriv.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.yourappsgeek.taxidriv.Controller.ScheduleRequest;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.adapters.HistoryRequestAdapter;
import com.yourappsgeek.taxidriv.interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.taxidriv.interfaces.buttoncallback_listener;
import com.yourappsgeek.taxidriv.models.HistoryItem;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;
import static com.yourappsgeek.taxidriv.Service.Constrants.userId;

public class RequestHistoryFragment extends Fragment implements ScheduleJobCallbackListener {
    private List<HistoryItem> historyItems;
    RecyclerView recyclerView;
    HistoryRequestAdapter adapter;
    ScheduleRequest scheduleRequest;
    ProgressDialog progressDialog;
    //updateStatuscController controller;
    SharedPreferences preferences;
    ImageView nodata;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_request_history, container, false);
        nodata = v.findViewById(R.id.nodata);
        progressDialog = UiUtils.getProgressDialog(getContext());

        if (!isConnected(getContext())) ;
        else {
            scheduleRequest = new ScheduleRequest(getContext(), this);
            // controller =new updateStatuscController(getContext(),this);
            ConfigInitView(v);

            if (userId!=0)
            scheduleRequest.JobsHistory();
        }
        return v;
    }

    private void ConfigInitView(View view) {
        historyItems = new ArrayList<>();

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        recyclerView = view.findViewById(R.id.historyItemView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        adapter = new HistoryRequestAdapter(historyItems, getContext(), R.layout.history_list_item, new buttoncallback_listener() {
            @Override
            public void onClick(View view, int possition) {
                // controller.JOBSHistory(jobid,1);
            }
        });
        recyclerView.setAdapter(adapter);


    }


    @Override
    public void onFetchProgress(HistoryItem historyItem) {
          progressDialog.show();
        adapter.addItem(historyItem);
    }

//    @Override
//    public void onUpdateStatusProgress(UpdateStatus status) {
//
//    }

    @Override
    public void onFetchComplete() {
        hideProgress();
        if (historyItems.size()==0){
            nodata.setVisibility(View.VISIBLE);
        }else {
            nodata.setVisibility(View.GONE);
        }
    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}

