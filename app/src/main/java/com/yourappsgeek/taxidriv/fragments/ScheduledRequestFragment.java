package com.yourappsgeek.taxidriv.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.utils.UiUtils;

public class ScheduledRequestFragment extends Fragment
{
    ProgressDialog progressDialog;
    Button contact,track;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v=  inflater.inflate(R.layout.fragment_scheduled_request, container, false);
        MapView mapView = v.findViewById(R.id.mapView);
        track=v.findViewById(R.id.button);
        contact=v.findViewById(R.id.button2);
        progressDialog = UiUtils.getProgressDialog(getContext());
        if (mapView != null) {
            // Initialise the MapView
            mapView.setClickable(false);
            mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            mapView.getMapAsync(new OnMapReadyCallback()
            {
                @Override
                public void onMapReady(GoogleMap googleMap)
                {
                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    try
                    {
                        MapsInitializer.initialize(getActivity());
                        LatLng sydney = new LatLng(-34, 151);
                        googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        hideProgress();
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                //do nothing, we want to suppress launching Google Maps
                            }
                        });
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Not Working!", Toast.LENGTH_SHORT).show();
            }
        });

        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Not Working!", Toast.LENGTH_SHORT).show();

            }
        });
        return v;
    }
    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
