package com.yourappsgeek.taxidriv.fragments;


import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yourappsgeek.taxidriv.R;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 12/10/2018.
 */

public class DeliverServiceBSFragment extends BottomSheetDialogFragment
{

    public DeliverServiceBSFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.deliver_task_bottom_sheet_fragment, container, false);
      v.findViewById(R.id.btnApprove).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showBottomSheetDialogFragment();
            }
        });
        return v;
    }

    public void showBottomSheetDialogFragment()
    {
        RateServiceBSFragment bottomSheetFragment = new RateServiceBSFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        this.dismiss();
    }


}