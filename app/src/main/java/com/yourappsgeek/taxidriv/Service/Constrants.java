package com.yourappsgeek.taxidriv.Service;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.libraries.places.api.model.Place;
import com.yourappsgeek.taxidriv.R;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

public class Constrants {
    public static double user_latitude;
    public static double user_longitude;
    public static CheckBox cbLane1, cbLane2, cbLane3;
    public static boolean check=false;
    public static String user_name;
    public static String profile_image;
    public static String Social_images;

    public static String FCM_Token;
    public static JSONArray data;
    public static Dialog dialog=null;
    public static List<Place.Field> fields= Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
    public static int AUTOCOMPLETE_REQUEST_CODE = 1;
    public static int AUTOCOMPLETE_REQUEST_CODE1 = 2;
    public static double latitude,longitude;
    public static float DEFAULT_ZOOM=15.5f;
    public static GoogleMap mMap;
    public static String Location_result;
    public static int status=0;
    public static int userId=0;
    public static int jobID=0;
    public static int edit_location=0;
    public static boolean dialogFlag=true;
    public static boolean editbutton=false;
    public static Uri imagePath;
    public static String realPath;
    public static int GALLERY = 1, CAMERA = 2;
    public static Dialog alertDialog;
    public static String title,servicePrice,customerID,customer_approvel;
    public static TextView edit_location_address;
    public static String api;
    public static String body;
    public static String ntitle;
    public static ProgressDialog progressDialog;
    public static String mesg;
    public static String User_ID,Provider_ID;

}
