package com.yourappsgeek.taxidriv.Service;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("customer/register")
    Call<ResponseBody> createUser(
            @Field("username") String fullname,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("radius") String radius,
            @Field("password") String password,
            @Field("fcm_token") String fcm_token);


    @FormUrlEncoded
    @POST("customer/login")
    Call<ResponseBody> userLogin(
            @Field("email") String email,
            @Field("password") String password,
            @Field("fcm_token") String token,
            @Field("lat") Double lat,
            @Field("lng") Double lng);



    @FormUrlEncoded
    @POST("customer/logout")
    Call<ResponseBody> userLogOut(
            @Field("fcm_token") String fcm_token);


    @POST("customer/socialLogin")
    Call<ResponseBody> SocialLogin(
            @Query("social_id") String social_id,
            @Query("username") String username,
            @Query("fcm_token") String fcm_token,
            @Query("email") String email,
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("avatar_file") String avatar_file,
            @Query("lat") Double lat,
            @Query("lng") Double lng);



    @GET("general/services")
    Call<ResponseBody> services(
            @Query("fcm_token") String fcm_token
    );


    @FormUrlEncoded
    @POST("jobs/store")
    Call<ResponseBody> jobcreate(
            @Field("title") String title,
            @Field("service_price") String service_price,
            @Field("lat") double lat,
            @Field("lng") double lng,
            @Field("location_address") String location_address,
            @Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("jobs/store")
    Call<ResponseBody> EditJob(
            @Field("title") String title,
            @Field("service_price") String service_price,
            @Field("lat") double lat,
            @Field("lng") double lng,
            @Field("location_address") String location_address,
            @Field("customer_id") String customer_id,
            @Field("id") String job_id,
            @Field("customer_approval") String customer_approvel,
            @Field("job_status") int job_status);


    @FormUrlEncoded
    @POST("jobs/checkCurrentJob")
    Call<ResponseBody> check_current_job(
            @Field("user_id") String user_id,
            @Field("type") String type);

    @FormUrlEncoded
    @POST("jobs/leftJob")
    Call<ResponseBody> cancle_job(
            @Field("job_id") int job_id);


    @FormUrlEncoded
    @POST("customer/nearByProviders")
    Call<ResponseBody> near_by_driver(
            @Field("lat") double lat,
            @Field("lng") double lng);

    @FormUrlEncoded
    @POST("customer/getUserStripDetails")
    Call<ResponseBody> payment(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("jobs/customerApprove")
    Call<ResponseBody> customer_approve(
            @Field("job_id") int job_id,
            @Field("approve") int approve,
            @Field("driver_rating") float driver_ratin);


    @FormUrlEncoded
    @POST("customer/addCard")
    Call<ResponseBody> addcard(
            @Field("user_id") int user_id,
            @Field("token") String token);


    @FormUrlEncoded
    @POST("customer/updateLatLng")
    Call<ResponseBody> update_latlng(
            @Field("lat") double latitude,
            @Field("lng") double longitude,
            @Field("fcm_token") String token);


    @FormUrlEncoded
    @POST("customer/removeStripCard")
    Call<ResponseBody> removeCard(
            @Field("user_id") String user_id,
            @Field("card_id") String card_id);

    @FormUrlEncoded
    @POST("customer/setDefaultStripCard")
    Call<ResponseBody> DefaultStripCard(
            @Field("user_id") String user_id,
            @Field("card_id") String card_id);


    @FormUrlEncoded
    @POST("customer/updateProfile")
    Call<ResponseBody> Phone_Update(
            @Field("user_id") String user_id,
            @Field("fcm_token") String fcm_token,
            @Field("phone") String phone);

    @Multipart
    @POST("customer/updateProfile")
    Call<ResponseBody> profileImage(
            @Part("user_id") int user_id,
            @Part("fcm_token") RequestBody fcm_token,
            @Part("first_name")RequestBody first_name,
            @Part("last_name")RequestBody last_name,
            @Part MultipartBody.Part image);

    @POST("customer/updatePassword")
    Call<ResponseBody> updatePassword(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("old_password") String oldP,
            @Query("new_password") String newP);

    @GET("general/limitations")
    Call<ResponseBody> limitations();

    @FormUrlEncoded
    @POST("customer/validateCoupons")
    Call<ResponseBody> validateCoupons(@Field("code") String code);

    @POST("jobs/checkPreviousJob")
    Call<ResponseBody> checkPreviousJob(
            @Query("user_id") String userId,
            @Query("type") String type);

    @POST("jobs/jobEditRequest")
    Call<ResponseBody> editRequestStatus(
            @Query("job_id") int jobID);



    @POST("insertIssue")
    Call<ResponseBody> help(
            @Query("user_id") int provider_id,
            @Query("user_type") String user_type,
            @Query("issue_name") String issue_name,
            @Query("issue_type") String issue_type,
            @Query("issue_description") String issue_description,
            @Query("status") String status);
    

    @POST("chat/send")
    Call<ResponseBody> sendMessage(
            @Query("to") String to,
            @Query("from") String from,
            @Query("message") String message);

    @POST("chat/get")
    Call<ResponseBody> getMessage(
            @Query("user1") String to,
            @Query("user2") String from,
            @Query("page") int page);

    @POST("customer/forgetpassword")
    Call<ResponseBody> forgetpassword(
            @Query("email") String email
    );

    @POST("customer/getforgetcode")
    Call<ResponseBody> getforgetcode(
            @Query("email") String email,
            @Query("code") String code

    );


    @POST("provider/updatePassword")
    Call<ResponseBody> ChangePassword(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("no_old") String oldP,
            @Query("new_password") String newP);
}