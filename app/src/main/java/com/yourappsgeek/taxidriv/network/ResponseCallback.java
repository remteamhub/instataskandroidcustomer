package com.yourappsgeek.taxidriv.network;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/6/2018.
 */
public interface ResponseCallback
{
    void onSuccess(String response);

    void onFailed(String msg);
}
