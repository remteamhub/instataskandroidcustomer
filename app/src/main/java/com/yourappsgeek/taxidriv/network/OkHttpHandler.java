package com.yourappsgeek.taxidriv.network;

import android.util.Log;


import com.yourappsgeek.taxidriv.config.NetworkConfig;
import com.yourappsgeek.taxidriv.executor.MainThread;
import com.yourappsgeek.taxidriv.mapping.EntityMappingProvider;
import com.yourappsgeek.taxidriv.mapping.ParsingException;
import com.yourappsgeek.taxidriv.requests.WebServiceRequest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/6/2018.
 */

public class OkHttpHandler
{

    private String token;
    private String contentType;

    private static class SingletonHolder
    {
        public static final OkHttpHandler INSTANCE = new OkHttpHandler();
    }

    private static OkHttpHandler ourInstance = null;

    private OkHttpHandler()
    {
    }

    public static OkHttpHandler getOurInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    public void sendGetRequest(String url, WebServiceRequest requestObj, ResponseCallback callback, MainThread mainThread)
    {
        Request req = getRequestBuilder(url, requestObj.getHeaderData()).build();
        try
        {
            run(req, callback, mainThread);
        } catch (IOException e)
        {
            callback.onFailed(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public void sendPostRequest(String url, WebServiceRequest request, ResponseCallback callback, MainThread mainThread)
    {
        try
        {
            MediaType contentType = MediaType.parse(NetworkConfig.CONTENT_TYPE_DEFAULT + "; charset=utf-8");

            String requestJson = EntityMappingProvider.get().transformToParsableData(request);
            RequestBody body = RequestBody.create(contentType, requestJson);

            Request request1 = getRequestBuilder(url, request.getHeaderData()).post(body).build();
            Log.w("RESPONSE", requestJson);

            run(request1, callback, mainThread);
        } catch (ParsingException e)
        {
            callback.onFailed(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e)
        {
            callback.onFailed(e.getLocalizedMessage());
            e.printStackTrace();
        }

    }

    public void sendPutRequest(String url, WebServiceRequest request, ResponseCallback callback, MainThread mainThread)
    {
        try
        {
            MediaType contentType = MediaType.parse(NetworkConfig.CONTENT_TYPE_DEFAULT + "; charset=utf-8");

            String requestJson = EntityMappingProvider.get().transformToParsableData(request);
            RequestBody body = RequestBody.create(contentType, requestJson);

            Request request1 = getRequestBuilder(url, request.getHeaderData()).put(body).build();
            Log.w("RESPONSE", requestJson);
            run(request1, callback, mainThread);
        } catch (ParsingException e)
        {
            callback.onFailed(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e)
        {
            callback.onFailed(e.getLocalizedMessage());
            e.printStackTrace();
        }

    }


    void run(Request request, final ResponseCallback callback, final MainThread mainThread) throws IOException
    {
        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(final Call call, final IOException e)
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {

                        callback.onFailed("Something went wrong, please try again.");
                        call.cancel();
                    }
                };
                mainThread.post(runnable);

            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException
            {

                final String myResponse = response.body().string();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        callback.onSuccess(myResponse);
                    }
                };
                mainThread.post(runnable);
            }
        });
    }
    
    private Request.Builder getRequestBuilder(String requestUrl, HashMap<String, String> headers)
    {
        Request.Builder reqBuilder = new Request.Builder().url(requestUrl);

        if (headers == null)
            return reqBuilder;

        for (Map.Entry<String, String> entry : headers.entrySet())
        {
            reqBuilder.addHeader(entry.getKey(), entry.getValue());
        }
        return reqBuilder;
    }


}