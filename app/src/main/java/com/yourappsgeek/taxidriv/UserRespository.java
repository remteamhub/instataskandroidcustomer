package com.yourappsgeek.taxidriv;

import com.yourappsgeek.taxidriv.config.NetworkConfig;
import com.yourappsgeek.taxidriv.config.WebService;
import com.yourappsgeek.taxidriv.network.NetworkResponseCallback;
import com.yourappsgeek.taxidriv.network.OkHttpHandler;
import com.yourappsgeek.taxidriv.network.ResponseCallback;
import com.yourappsgeek.taxidriv.parser.JsonParserImp;
import com.yourappsgeek.taxidriv.requests.RegisterRequest;
import com.yourappsgeek.taxidriv.requests.SignInRequest;
import com.yourappsgeek.taxidriv.response.RegisterResponse;
import com.yourappsgeek.taxidriv.response.SignInResponse;
import com.yourappsgeek.taxidriv.threading.MainThreadImpl;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/20/2018.
 */
public class UserRespository
{

    public void signIn(SignInRequest request, final NetworkResponseCallback<SignInResponse> callback)
    {
        String url = NetworkConfig.SERVER_URL + WebService.LOGIN_CUSTOMER;
        OkHttpHandler.getOurInstance().sendPostRequest(url, request, new ResponseCallback()
        {
            @Override
            public void onSuccess(final String response)
            {
                SignInResponse response1 = JsonParserImp.getOurInstance().parserSignIn(response);
                callback.onSuccess(response1);
            }

            @Override
            public void onFailed(final String msg)
            {
                callback.onFailure(922, msg);

            }
        }, MainThreadImpl.getInstance());

    }

    public void signUp(RegisterRequest request, final NetworkResponseCallback<RegisterResponse> callback)
    {
        String url = NetworkConfig.SERVER_URL + WebService.REGISTER_CUSTOMER;
        OkHttpHandler.getOurInstance().sendPostRequest(url, request, new ResponseCallback()
        {
            @Override
            public void onSuccess(final String response)
            {
                RegisterResponse response1 = JsonParserImp.getOurInstance().parserRegister(response);
                callback.onSuccess(response1);
            }
            @Override
            public void onFailed(final String msg)
            {
                callback.onFailure(922, msg);
            }
        }, MainThreadImpl.getInstance());

    }
}
