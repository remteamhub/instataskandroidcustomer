package com.yourappsgeek.taxidriv.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.EMAIL;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.FCM_TOKEN;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.LOCATION;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.LOGGED_IN_PREF;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.PAYMENT_M;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.PHONE_NUM;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.PRICE;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.PROFILE_IMAGE;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.SERVICE_LANE;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.USER_ID;
import static com.yourappsgeek.taxidriv.SharedPreferences.PreferencesUtility.USER_PROFILE;

public class SaveSharedPreference {



    static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Set the Login Status
     * @param context
     * @param loggedIn
     */
    public static void setLoggedIn(Context context, boolean loggedIn) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.apply();
    }

    public static void setUserPro(Context context, String user_profile) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(USER_PROFILE, user_profile);
        editor.apply();
    }




    public static void setUserId(Context context, String user_id) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(USER_ID, user_id);
        editor.apply();
    }

    public static void setFCM_Token(Context context, String fcm_token) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(FCM_TOKEN, fcm_token);
        editor.apply();
    }

    public static void setService_Lane(Context context, String service_lane) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(SERVICE_LANE, service_lane);
        editor.apply();
    }

    public static void setPrice(Context context, String price) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PRICE, price);
        editor.apply();
    }

    public static void setLocation(Context context, String location) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(LOCATION, location);
        editor.apply();
    }

    public static void setPayment_M(Context context, String payment) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PAYMENT_M, payment);
        editor.apply();
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public static void setPhone_Num(Context context, String phone) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PHONE_NUM, phone);
        editor.apply();
    }
    /**
     * Get the Login Status
     * @param context
     * @return boolean: login status
     */
    public static boolean getLoggedStatus(Context context) {
        return getPreferences(context).getBoolean(LOGGED_IN_PREF, false);
    }

    public static String getUserStatus(Context context) {
        return getPreferences(context).getString(USER_PROFILE, "");
    }




    public static String getUser_ID(Context context) {
        return getPreferences(context).getString(USER_ID, "");
    }

    public static String getFCM_Token(Context context) {
        return getPreferences(context).getString(FCM_TOKEN, "");
    }

    public static String getService_Lane(Context context) {
        return getPreferences(context).getString(SERVICE_LANE, "");
    }
    public static String getPrice(Context context) {
        return getPreferences(context).getString(PRICE, "");
    }

    public static String getLocation(Context context) {
        return getPreferences(context).getString(LOCATION, "");
    }

    public static String getPayment_M(Context context) {
        return getPreferences(context).getString(PAYMENT_M, "");
    }

    public static void setProfile_Image(Context context, String pro_img) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PROFILE_IMAGE, pro_img);
        editor.apply();
    }

    public static String getPhone_Num(Context context) {
        return getPreferences(context).getString(PHONE_NUM, "");
    }

    public static String getEmail(Context context) {
        return getPreferences(context).getString(EMAIL, "");
    }

    public static String getProfile_Image(Context context) {
        return getPreferences(context).getString(PROFILE_IMAGE, "");
    }
}
