package com.yourappsgeek.taxidriv.SharedPreferences;

public class PreferencesUtility {

    // Values for Shared Prefrences
    public static final String LOGGED_IN_PREF = "logged_in_status";
    public static final String USER_PROFILE="user_profile";
    public static final String USER_ID="user_id";
    public static final String FCM_TOKEN="fcm_token";
    public static String SERVICE_LANE="service_lane";
    public static String PRICE="price";
    public static String LOCATION="location";
    public static String PAYMENT_M="payment_m";
    public static String PHONE_NUM="phone_number";
    public static String EMAIL="email";

    public static final String PROFILE_IMAGE="profile_image";

}
