package com.yourappsgeek.taxidriv.activities;

import android.annotation.SuppressLint;
import android.content.Intent;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIUrl;

import java.text.SimpleDateFormat;
import java.util.Locale;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DetailsHistoryRequest extends AppCompatActivity
{

    TextView priceLane,cardnumber,providerName,time,issue_report;
    MaterialRatingBar materialRatingBar;
    ImageView beforeImage,afterImage;
    double latitude,longitude;
    float rate;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_history_request);

        priceLane=findViewById(R.id.textView17);
        cardnumber=findViewById(R.id.textView16);
        providerName=findViewById(R.id.textView19);
        materialRatingBar=findViewById(R.id.ratingBar2);
        beforeImage=findViewById(R.id.imageView6);
        afterImage=findViewById(R.id.imageView7);
        time=findViewById(R.id.textView18);
        findViewById(R.id.textView20).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailsHistoryRequest.this,HelpActivity.class));

            }
        });

        long unixTime= Long.parseLong(getIntent().getStringExtra("time"));
        java.util.Date d = new java.util.Date(unixTime*1000L);
        String itemDateStr = new SimpleDateFormat("hh:mm aa", Locale.US).format(d);
        time.setText(itemDateStr);


        priceLane.setText(getIntent().getStringExtra("price")+"$"+" / "+getIntent().getStringExtra("lane"));
        providerName.setText(getIntent().getStringExtra("provideName"));
        rate= Float.parseFloat(getIntent().getStringExtra("rating"));
        materialRatingBar.setRating(rate);
        String before=APIUrl.IMAGE_BASE_URL +getIntent().getStringExtra("before_img");
        String after=APIUrl.IMAGE_BASE_URL +getIntent().getStringExtra("after_img");




        if (before.equals(null) || before.equals("")){
            Picasso.with(getApplicationContext()).load(R.drawable.no_uploaded).into(beforeImage);
        }else {
            Picasso.with(getApplicationContext()).load(before).into(beforeImage);
        }

        if (after.equals(null) || after.equals("")){
            Picasso.with(getApplicationContext()).load(R.drawable.no_uploaded).into(afterImage);
        }else {
            Picasso.with(getApplicationContext()).load(after).into(afterImage);
        }



        Log.e("images",before+" "+after);

        latitude=getIntent().getDoubleExtra("lat",0.0);
        longitude=getIntent().getDoubleExtra("long",0.0);



         MapView mapView = findViewById(R.id.mapView3);
         if (mapView != null) {
             // Initialise the MapView
             mapView.onCreate(null);
             // Set the map ready callback to receive the GoogleMap object
             mapView.getMapAsync(new OnMapReadyCallback()
             {
                 @Override
                 public void onMapReady(GoogleMap googleMap)
                 {
                     googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                     // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                     try
                     {
                         MapsInitializer.initialize(DetailsHistoryRequest.this);
                         LatLng sydney = new LatLng(latitude, longitude);
                         googleMap.addMarker(new MarkerOptions().position(sydney).title("You are here!"));
                         googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,15.7f));
                         googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                             @Override
                             public void onMapClick(LatLng latLng) {
                                 //do nothing, we want to suppress launching Google Maps
                             }
                         });
                     } catch (Exception e)
                     {
                         e.printStackTrace();
                     }
                 }
             });
         }
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);

        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);

        title.setText(getString(R.string.title_activity_my_request));

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
