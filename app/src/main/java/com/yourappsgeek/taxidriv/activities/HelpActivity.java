package com.yourappsgeek.taxidriv.activities;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.response.ParseJson;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;
import static com.yourappsgeek.taxidriv.Service.Constrants.userId;

public class HelpActivity extends AppCompatActivity
{

    EditText edtIssueKind,edtIssueType,edtDescription;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        edtIssueKind=findViewById(R.id.edtIssueKind);
        edtIssueType=findViewById(R.id.edtIssueType);
        edtDescription=findViewById(R.id.edtDescription);
        btnSubmit=findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String kind = edtIssueKind.getText().toString();
                String type = edtIssueType.getText().toString();
                String discription = edtDescription.getText().toString();
                int text_length=discription.length();
                if (kind.matches("")) {
                    edtIssueKind.setError("Please add issue kind!");
                    return;
                }else if (type.matches("")){
                    edtIssueType.setError("Please add issue type!");
                    return;
                }else if (discription.matches("")){
                    edtDescription.setError("Please add issue discription!");

                    return;
                }else if (text_length<20){
                    edtDescription.setError("Minimum 20 character discription!");
                    return;
                }else {
                    if (!isConnected(getApplicationContext())) ;
                    else {
                        if (userId!=0)
                        ParseJson.Help(HelpActivity.this, Constrants.userId, "Customer", kind, type, discription, "Satisfied");
                        else
                            Toast.makeText(HelpActivity.this, "SignIn/Signup to use this feature.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);

        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);

        title.setText(getString(R.string.title_activity_help));

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
