package com.yourappsgeek.taxidriv.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.adapters.PaymentMethodAdapter;
import com.yourappsgeek.taxidriv.adapters.RecyclerItemTouchHelper;
import com.yourappsgeek.taxidriv.interfaces.CallActivity;
import com.yourappsgeek.taxidriv.interfaces.RecyclerItemTouchHelperListener;
import com.yourappsgeek.taxidriv.models.Payment_model;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;

public class Payment_Method extends AppCompatActivity implements RecyclerItemTouchHelperListener, CallActivity {

    Button payment;
    RecyclerView payment_recycler;
    PaymentMethodAdapter adapter;
    List<Payment_model> modelRecyclerArrayList;
    RelativeLayout snakebar;
    ProgressDialog progressDialog;
    String user_id;
    ImageView nodata;
    SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__method);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);
        snakebar = findViewById(R.id.snakebar);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        user_id = SaveSharedPreference.getUser_ID(getApplicationContext());
        progressDialog = UiUtils.getProgressDialog(this);
        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);
        title.setText(getString(R.string.payment_method));
        nodata=findViewById(R.id.nodata);
        modelRecyclerArrayList = new ArrayList<>();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }

        payment_recycler = findViewById(R.id.payment_recycler);
        if (!isConnected(getApplicationContext()));
        else {
            fetchJSON(user_id);
        }


        payment = findViewById(R.id.payment);

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Addpayment();
            }
        });


            adapter = new PaymentMethodAdapter(Payment_Method.this, modelRecyclerArrayList, this, new PaymentMethodAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Payment_model item) {

                }

                @Override
                public void OnCheckClick(int possition, CompoundButton buttonView, boolean isChecked, Payment_model item) {


                    if (isChecked) {
                        String cardID = item.getId();
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            DefaultCard(user_id, cardID);
                        }
//                    Toast.makeText(Payment_Method.this, "check", Toast.LENGTH_SHORT).show();
                    } else {

//                    Toast.makeText(Payment_Method.this, "Uncheck", Toast.LENGTH_SHORT).show();

                    }
                }


            });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(Payment_Method.this, MainActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void Addpayment() {
        startActivity(new Intent(getApplicationContext(), PaymentActivity.class));
        finish();
    }


    private void fetchJSON(String user_id) {


        progressDialog.show();
        APIService api = APIUrl.getClient().create(APIService.class);

        Call<ResponseBody> call = api.payment(user_id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    //getting the whole json object from the response
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        hideProgress();
                        //    String ID = json.getString("id");
                        JSONObject source = json.getJSONObject("sources");
                        JSONArray data = source.getJSONArray("data");


                        for (int i = 0; i < data.length(); i++) {


                            JSONObject dataobj = data.getJSONObject(i);

                            //   modelRecycler.setCard_type(dataobj.getString(""));


                            Payment_model modelRecycler = new Payment_model(
                                    dataobj.getString("exp_month"),
                                    dataobj.getString("exp_year"),
                                    dataobj.getString("last4"),
                                    String.valueOf(i + 1),
                                    dataobj.getString("id")

                            );


                            modelRecyclerArrayList.add(modelRecycler);


                        }


                        // refreshing recycler view
                        Gson gson = new Gson();
                        String jsonn = gson.toJson(modelRecyclerArrayList);
                        SharedPreferences.Editor editor = preferences.edit();
                        Log.e("payment", jsonn);
                        editor.putString("paymentList", jsonn);
                        editor.commit();
                        payment_recycler.setAdapter(adapter);
                        payment_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                        if (modelRecyclerArrayList.size()>0) {
                            nodata.setVisibility(View.GONE);
                        }
                        else
                            nodata.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();



                    } else {
                        hideProgress();
                        Toast.makeText(Payment_Method.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();



            }
        });

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(payment_recycler);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof PaymentMethodAdapter.PaymentRequestHolder) {
            // get the removed item name to display it in snack bar
            String name = modelRecyclerArrayList.get(viewHolder.getAdapterPosition()).getCard_number();

            // backup of removed item for undo purpose
            final Payment_model deletedItem = modelRecyclerArrayList.get(viewHolder.getAdapterPosition());

            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view

            adapter.removeItem(viewHolder.getAdapterPosition(), String.valueOf(deletedItem.getId()));

        }
    }


    @Override
    public void removeCard(String item) {

        showDialog(this, item);
        Log.d("card_id", item);
//        RemoveCard(user_id,item);
    }


    public void showDialog(Activity activity, final String card_id) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.newcustom_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        FrameLayout mDialogNo = dialog.findViewById(R.id.frmNo);
        mDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelRecyclerArrayList.clear();
                if (!isConnected(getApplicationContext()));
                else {
                    fetchJSON(user_id);
                }
                // Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        FrameLayout mDialogOk = dialog.findViewById(R.id.frmOk);
        mDialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isConnected(getApplicationContext()));
                else {
                    RemoveCard(user_id, card_id);
                }
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void RemoveCard(String user_id, String card_id) {

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.removeCard(user_id, card_id);

        //calling the api
        try {
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    if (response.isSuccessful()) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("paymentList", "");
                        editor.commit();
                        hideProgress();
                        SaveSharedPreference.setPayment_M(getApplicationContext(), "");
                        Toast.makeText(Payment_Method.this, "Card deleted successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgress();
                        Toast.makeText(Payment_Method.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    if (modelRecyclerArrayList.size()>0) {
                        nodata.setVisibility(View.GONE);
                    }
                    else
                        nodata.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }

    private void DefaultCard(final String user_id, String card_id) {

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.DefaultStripCard(user_id, card_id);

        //calling the api
        try {
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {


                        modelRecyclerArrayList.clear();
                        if (!isConnected(getApplicationContext()));
                        else {
                            fetchJSON(user_id);
                        }
                        hideProgress();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Toast.makeText(Payment_Method.this, "Card deleted successfully!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Payment_Method.this, MainActivity.class));
        finish();

    }
}
