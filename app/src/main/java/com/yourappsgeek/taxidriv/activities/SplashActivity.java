package com.yourappsgeek.taxidriv.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mAuth = FirebaseAuth.getInstance();

        TimerTask navigateTask = new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(SaveSharedPreference.getLoggedStatus(getApplicationContext())) {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            gotoLoginScreen();
                        }

                    }
                });

            }
        };
        new Timer().schedule(navigateTask, 1200);

        if((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0){

        }else {
            if (getIntent().getExtras() != null) {
                String api = getIntent().getExtras().getString("api");
                String body = getIntent().getExtras().getString("body");
                String title = getIntent().getExtras().getString("title");

                Log.e("notificarionData", api + " " + body);
                Constrants.mesg = body;
                if (api != null && body != null && title != null) {
                    Constrants.api = api;
                    Constrants.body = body;
                    Constrants.ntitle = title;
                }
            }
        }
    }



    private void gotoLoginScreen() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }
}
