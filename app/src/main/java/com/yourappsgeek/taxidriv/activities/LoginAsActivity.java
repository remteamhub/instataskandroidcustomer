package com.yourappsgeek.taxidriv.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.yourappsgeek.taxidriv.R;

public class LoginAsActivity extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_as);

        //add click listeners
        findViewById(R.id.btnCustomer).setOnClickListener(this);
        findViewById(R.id.btnProvider).setOnClickListener(this);


    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnCustomer:
            {
                startActivity(new Intent(LoginAsActivity.this,LoginActivity.class));
                finish();
                break;
            }
            case R.id.btnProvider:
            {
                startActivity(new Intent(LoginAsActivity.this,LoginActivity.class));
                finish();
                break;
            }
        }
    }
}
