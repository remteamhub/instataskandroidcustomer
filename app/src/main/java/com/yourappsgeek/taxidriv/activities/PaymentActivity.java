package com.yourappsgeek.taxidriv.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;
import static com.yourappsgeek.taxidriv.Service.Constrants.userId;

public class PaymentActivity extends AppCompatActivity
{
    CardInputWidget mCardInputWidget;
    TextInputLayout cardName;
    ProgressDialog progressDialog;
    ConstraintLayout snakebar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mCardInputWidget = findViewById(R.id.card_input_widget);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);
        progressDialog = UiUtils.getProgressDialog(this);
        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);
        cardName=findViewById(R.id.textInputLayout);
        snakebar=findViewById(R.id.snakebar);
        title.setText(getString(R.string.payment));

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }





        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (userId!=0) {
                    if (!isConnected(getApplicationContext())) ;
                    else {

                        progressDialog.show();
                        Stripe stripe = new Stripe(getApplicationContext(), "pk_test_51JocM3KcDx1GgYEo4D7i7uRULkpO1rhVTvcaICVXUI6sPkkuh8mxcSbvYZzqF99emVMDsDgQcolVCeblkOoojyK600UUJ1M5Oy");
                        Card card = mCardInputWidget.getCard();
                        if (card == null) {
                            // Do not continue token creation.
                            hideProgress();
                            Toast.makeText(PaymentActivity.this, "Invalid card", Toast.LENGTH_SHORT).show();
                        } else {
                            stripe.createToken(
                                    card,
                                    new TokenCallback() {
                                        public void onSuccess(Token token) {
                                            // Send token to your server

                                            String tokn = token.getId();

                                            int id1 = Integer.parseInt(SaveSharedPreference.getUser_ID(getApplicationContext()));
                                            Log.e("token", tokn);
                                            if (!isConnected(getApplicationContext())) ;
                                            else {
                                                addcard(id1, tokn);
                                            }
                                        }

                                        public void onError(Exception error) {
                                            // Show localized error message
                                            hideProgress();
                                            Toast.makeText(PaymentActivity.this, String.valueOf(error), Toast.LENGTH_SHORT).show();

                                        }
                                    }
                            );
                        }


                    }
                }else {
                    Snackbar.make(snakebar,"SignIn/SignUp to use this feature.",Snackbar.LENGTH_INDEFINITE).setAction("Login", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(PaymentActivity.this,LoginActivity.class));
                            finish();
                        }
                    }).show();
                }
            }
        });
    }



    void addcard(int user_id, String token) {

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.addcard(user_id,token);



        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");

                        Toast.makeText(PaymentActivity.this, "Card added successfully!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), Payment_Method.class));
                        finish();
                        hideProgress();
                    }else {
                        hideProgress();
                        Toast.makeText(PaymentActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }
    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(PaymentActivity.this, Payment_Method.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PaymentActivity.this, Payment_Method.class));
        finish();

    }
}
