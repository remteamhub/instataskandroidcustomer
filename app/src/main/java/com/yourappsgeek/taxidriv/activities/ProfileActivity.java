package com.yourappsgeek.taxidriv.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yourappsgeek.taxidriv.CircleImageView;
import com.yourappsgeek.taxidriv.Controller.ScheduleRequest;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;
import static com.yourappsgeek.taxidriv.Service.Constrants.CAMERA;
import static com.yourappsgeek.taxidriv.Service.Constrants.GALLERY;
import static com.yourappsgeek.taxidriv.Service.Constrants.Social_images;
import static com.yourappsgeek.taxidriv.Service.Constrants.alertDialog;
import static com.yourappsgeek.taxidriv.Service.Constrants.userId;

public class ProfileActivity extends AppCompatActivity {
    TextView phone_num, email, rate_value, edit_password;
    CircleImageView header_img;
    MaterialRatingBar materialRatingBar;
    float rating;
    ImageView edit_img;
    EditText profile_name;
    Button update;
    String oldPassword;
    SharedPreferences preferences;
    File file;
    ConstraintLayout snakebar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String rate_val = preferences.getString("rate_value", "");
        if (!rate_val.equals("")) {
            rating = Float.parseFloat(rate_val);
        }
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);
        String name = Constrants.user_name;
        String phone = SaveSharedPreference.getPhone_Num(getApplicationContext());
        String Email = SaveSharedPreference.getEmail(getApplicationContext());
        profile_name = findViewById(R.id.tvName);
        edit_password = findViewById(R.id.tvEditPassword);
        edit_img = findViewById(R.id.imgPicImg);
        update = findViewById(R.id.update);
        snakebar=findViewById(R.id.snakebar);

        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);
        phone_num = findViewById(R.id.tvContactNumber);

        email = findViewById(R.id.tvEmailAddress);

        title.setText(getString(R.string.title_activity_profile));
        header_img = findViewById(R.id.imgPicImage);
        materialRatingBar = findViewById(R.id.rbRating);
        rate_value = findViewById(R.id.textView);


        if (userId == 0) {
            phone_num.setText("123456789");
            email.setText("abc@example.com");
            rate_value.setText("5.0" + "/5.0");
            profile_name.setText("Demo user");
        } else {
            phone_num.setText(phone);
            email.setText(Email);
            profile_name.setText(name);
            if (rating != 0 || rating != 0.0) {
                materialRatingBar.setRating(rating);
                rate_value.setText(rating + "/5.0");
            } else {
                materialRatingBar.setRating(5.0f);
                rate_value.setText(5.0f + "/5.0");
            }
        }


        try {
            Social_images = preferences.getString("Social_images", "");
            String pro_img = preferences.getString("profile_image", "");
            String img = APIUrl.IMAGE_BASE_URL + pro_img;
            if (!pro_img.equals("")) {
                Glide.with(getApplicationContext()).load(img).into(header_img);
            } else if (!Social_images.equals("")) {
                Glide.with(getApplicationContext()).load(Social_images).into(header_img);
            } else {
                Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(header_img);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }


        edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getImage();
            }
        });

        edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatepassDialog();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileUpdate();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        int id = item.getItemId();
        if (id == R.id.nav_edit) {
            if (userId == 0) {

                Snackbar.make(snakebar,"SignIn/SignUp to use this feature.",Snackbar.LENGTH_INDEFINITE).setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProfileActivity.this,LoginActivity.class));
                        finish();
                    }
                }).show();
               // Toast.makeText(ProfileActivity.this, "SignIn/Signup to use this feature.", Toast.LENGTH_SHORT).show();

                edit_img.setVisibility(View.GONE);
                edit_password.setVisibility(View.GONE);
                update.setVisibility(View.GONE);
                profile_name.setEnabled(false);
            } else {
                edit_img.setVisibility(View.VISIBLE);
                edit_password.setVisibility(View.VISIBLE);
                update.setVisibility(View.VISIBLE);
                profile_name.setEnabled(true);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void getImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(galleryIntent, GALLERY);

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    header_img.setImageBitmap(bitmap);
                    //Uri imagePath = getImageUri(getApplicationContext(), bitmap);
                    // realPath = getRealPathFromURI(getApplicationContext(),imagePath);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), bitmap);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    file = new File(getRealPathFromURI(tempUri));


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");

//
            header_img.setImageBitmap(thumbnail);
            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(getApplicationContext(), thumbnail);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            file = new File(getRealPathFromURI(tempUri));
        }

    }


    void updatepassDialog() {
        alertDialog = new Dialog(ProfileActivity.this);
        alertDialog.setContentView(R.layout.update_pass);
        alertDialog.setCancelable(false);


        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(this);
        oldPassword = preferences1.getString("loginPassword", "");


        final LinearLayout oldLayout = alertDialog.findViewById(R.id.old_pass_layout);
        final LinearLayout newLayout = alertDialog.findViewById(R.id.new_pass_layout);
        final EditText oldPass = alertDialog.findViewById(R.id.old_pass);
        final EditText newPass = alertDialog.findViewById(R.id.new_pass);
        final EditText confirmPass = alertDialog.findViewById(R.id.confirm_pass);
        Button oldCancel = alertDialog.findViewById(R.id.old_cancel);
        Button oldSubmit = alertDialog.findViewById(R.id.old_submit);
        Button newCancel = alertDialog.findViewById(R.id.new_cancel);
        Button newSubmit = alertDialog.findViewById(R.id.new_submit);

        oldCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        oldSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPass.length() < 5) {
                    Toast.makeText(ProfileActivity.this, "Password is not Match", Toast.LENGTH_SHORT).show();
                } else if (oldPass.getText().toString().equals(oldPassword)) {
                    newLayout.setVisibility(View.VISIBLE);
                    oldLayout.setVisibility(View.GONE);
                } else {
                    Toast.makeText(ProfileActivity.this, "Password is not Match", Toast.LENGTH_SHORT).show();
                }
            }
        });
        newCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newLayout.setVisibility(View.GONE);
                oldLayout.setVisibility(View.VISIBLE);
            }
        });
        newSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPass.length() < 7) {
                    Toast.makeText(ProfileActivity.this, "8 characters are required", Toast.LENGTH_SHORT).show();
                } else if (newPass.getText().toString().equals(confirmPass.getText().toString())) {

                    if (!isConnected(ProfileActivity.this)) ;

                    else {
                        // showProgress();
                        ScheduleRequest.updatePassword(getApplicationContext(), userId, Constrants.FCM_Token, oldPass.getText().toString(), newPass.getText().toString());
                    }

                } else {
                    Toast.makeText(ProfileActivity.this, "Confirm Password not match with new Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }


    private void profileUpdate() {


        String fullname = null;
        final String fName;
        final String lName;
        fullname = profile_name.getText().toString();
        int firstSpace = fullname.indexOf(" ");
        if (fullname.isEmpty()) {
            Toast.makeText(ProfileActivity.this, "Name Field Can't be empty", Toast.LENGTH_SHORT).show();
            return;
        } else if (firstSpace < 0) {
            Toast.makeText(ProfileActivity.this, "Please Enter full name", Toast.LENGTH_SHORT).show();
            return;
        } else {
            fName = fullname.substring(0, firstSpace);
            lName = fullname.substring(firstSpace).trim();
            if (fName.equals(lName)) {
                Toast.makeText(ProfileActivity.this, "Please Correct your name", Toast.LENGTH_SHORT).show();
                return;
            } else if (lName.length() < 2) {
                Toast.makeText(ProfileActivity.this, "Please write your proper name", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //String full_name=profile_name.getText().toString();

        // Create a request body with file and image media type
        RequestBody fileReqBody = null;
        if (file != null) {
            fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        }
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = null;
        if (fileReqBody != null) {
            part = MultipartBody.Part.createFormData("avatar_file", file.getName(), fileReqBody);
        }
        RequestBody first_name = null;
        if (fName != null) {
            first_name = RequestBody.create(MediaType.parse("text/plain"), fName);
        }
        RequestBody last_name = null;
        if (lName != null) {
            last_name = RequestBody.create(MediaType.parse("text/plain"), lName);
        }
        RequestBody token = null;
        if (Constrants.FCM_Token != null) {
            token = RequestBody.create(MediaType.parse("text/plain"), Constrants.FCM_Token);

        }

        ScheduleRequest.updateProfileImage(getApplicationContext(), userId, token, first_name, last_name, part);
        edit_img.setVisibility(View.GONE);
        edit_password.setVisibility(View.GONE);
        update.setVisibility(View.GONE);
        profile_name.setEnabled(false);


    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        Objects.requireNonNull(cursor).moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

//    public String getRealPathFromURI(Context context,Uri uri) {
//        if (uri == null) {
//            return null;
//        }
//        Cursor cursor = null;
//        String result = null;
//        int column_index = 0;
//        try {
//            String[] projection = {MediaStore.Images.Media.DATA};
//            cursor = context.getContentResolver().query(uri, projection, null, null, null);
//
//            if (cursor != null) {
//                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                cursor.moveToFirst();
//                result=cursor.getString(column_index);
//            }
//
//        }catch (Exception e){
//
//            e.printStackTrace();
//        }finally {
//            try {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//
//            } catch (Exception e) {
//                Log.e("While closing cursor", String.valueOf(e));
//            }
//        }
//
//
//
//        return result;
//    }

}
