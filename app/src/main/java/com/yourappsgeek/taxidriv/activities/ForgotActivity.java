package com.yourappsgeek.taxidriv.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.yourappsgeek.taxidriv.Controller.DialogController;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity {
    ConstraintLayout snake_bar;
    AppCompatEditText editText;
    TextView textView;
    Button button;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=sharedPreferences.edit();
        Constrants.progressDialog = UiUtils.getProgressDialog(ForgotActivity.this);
        initView();
    }

    private void initView(){
        editText=findViewById(R.id.edtEmail);
        button=findViewById(R.id.btnSignIn);
        snake_bar=findViewById(R.id.snakebar);
        textView=findViewById(R.id.code_note);
        editText.setHint("Enter Email Address");
       btn();
    }
    private void btn(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                    Validation();
               
            }
        });
    }

    private void Validation(){
        if (TextUtils.isEmpty(editText.getText().toString())){
            editText.setError("Not Empty!");
        }else {

            if (button.getText().toString().contains("Forgot")) {
                String email=editText.getText().toString();
                editor.putString("email",email).commit();
            ChangePasswordApi(email);
                Constrants.progressDialog.show();
            }else {
               String code=editText.getText().toString();
               String email=sharedPreferences.getString("email","");
                getforgetcode(email,code);
                Constrants.progressDialog.show();
            }
        }
    }


    private void ChangePasswordApi(String email){


            try {
                Constrants.progressDialog.show();
            }catch (Exception e){
                e.printStackTrace();
            }

            APIService request = APIUrl.getClient().create(APIService.class);
            Call<ResponseBody> call = request.forgetpassword(email);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            JSONObject json = new JSONObject(response.body().string());
                            Log.d("Response", json + "");
                            int code = json.getInt("code");
                            if (json.get("data") instanceof JSONObject && code == 200) {
                                editText.setText("");
                                editText.setHint("Enter Code");
                                button.setText("Confirm");
                                textView.setVisibility(View.VISIBLE);

                            }
                        } else {
                            Snackbar.make(snake_bar, response.message(), Snackbar.LENGTH_LONG).show();
                        }
                        hideProgress();
                    } catch (JSONException e) {

                        e.printStackTrace();
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }


    private void getforgetcode(String email, String code){

        try {
            Constrants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        APIService request = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = request.getforgetcode(email,code);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data=json.getJSONObject("data");
                            String validity=data.getString("validity");

                            if (validity.equals("false")){
                                 Snackbar.make(snake_bar, json.getString("error_msg"), Snackbar.LENGTH_LONG).show();
                            }else {
                                int user_id=data.getInt("user_id");

                                DialogController.changepass_dialog(ForgotActivity.this,user_id);
                            }
                           // Snackbar.make(snake_bar, response.message(), Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(snake_bar, response.message(), Snackbar.LENGTH_LONG).show();
                    }
                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    void hideProgress() {
        if (Constrants.progressDialog != null && Constrants.progressDialog.isShowing())
            Constrants.progressDialog.dismiss();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

}
