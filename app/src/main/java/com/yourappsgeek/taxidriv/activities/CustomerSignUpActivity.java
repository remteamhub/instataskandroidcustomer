package com.yourappsgeek.taxidriv.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.socialIntegrator.social.AccountKitIntegrator;
import com.yourappsgeek.taxidriv.socialIntegrator.social.FacebookIntegrator;
import com.yourappsgeek.taxidriv.socialIntegrator.social.SocialCallback;
import com.yourappsgeek.taxidriv.socialIntegrator.social.TwitterIntegrator;
import com.yourappsgeek.taxidriv.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.location.SimpleLocation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;

public class CustomerSignUpActivity extends AppCompatActivity implements View.OnClickListener {
    AccountKitIntegrator integrator;
    FacebookIntegrator facebookIntegrator;
    CallbackManager callbackManager;
    TwitterIntegrator twitterIntegrator;

    @BindView(R.id.twitterLogin)
    TwitterLoginButton twitterLogin;

    private ProgressDialog progressDialog;
    @BindView(R.id.edtFullName)
    EditText edtFullName;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.edtPassword)
    EditText edtPassword;

    @BindView(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;

    @BindView(R.id.wrapperFullName)
    TextInputLayout wrapperFullName;

    @BindView(R.id.wrapperEmail)
    TextInputLayout wrapperEmail;

    @BindView(R.id.wrapperPassword)
    TextInputLayout wrapperPassword;

    @BindView(R.id.wrapperConfirmPassword)
    TextInputLayout wrapperConfirmPassword;

    @BindView(R.id.cbTerms)
    CheckBox cbTerms;

    @BindView(R.id.fblogin)
    LoginButton fblogin;

    String fcm_token;
    final String radious = "3";
    private String phone1;
    double latitide, longitude;
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    private SimpleLocation location;
    private String userID;
    private String Fullname,Firstname,Lastname,Email,Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_signup);
        ButterKnife.bind(this);
        //add click listeners
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(CustomerSignUpActivity.this);
        findViewById(R.id.btnSignUp).setOnClickListener(this);
        findViewById(R.id.btnFacebook).setOnClickListener(this);
        findViewById(R.id.btnTwitter).setOnClickListener(this);
        findViewById(R.id.txtAlreadyHaveAccount).setOnClickListener(this);
        preference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = preference.edit();

        location = new SimpleLocation(this);
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }
        latitide = location.getLatitude();
        longitude = location.getLongitude();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp: {
                if (!isConnected(getApplicationContext())) ;
                else {
                    doSignUp();
                }
                break;
            }
            case R.id.btnFacebook: {
                if (!isConnected(getApplicationContext())) ;
                else {
                    doFbLogin();

                }
                break;
            }
            case R.id.btnTwitter: {
                if (!isConnected(getApplicationContext())) ;
                else {
                    doTwitterLogin();
                }
                break;
            }
            case R.id.txtAlreadyHaveAccount: {
                startActivity(new Intent(CustomerSignUpActivity.this, LoginActivity.class));
                finish();
                break;
            }
        }
    }

    public void doTwitterLogin() {
        twitterIntegrator = new TwitterIntegrator(twitterLogin, new SocialCallback() {

            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
                try {

                    if (!isConnected(getApplicationContext())) ;
                    else {
                        setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, latitide, longitude);
                    }

                    // setSocialUser(SocialId,username,fcm_token,email,first_name,last_name,image,"31.4351714","74.4262213");
                } catch (Exception e) {
                    Toast.makeText(CustomerSignUpActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(CustomerSignUpActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
            }
        });
        twitterIntegrator.login();
    }


    public void doFbLogin() {
        facebookIntegrator = new FacebookIntegrator(fblogin, callbackManager, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String SocialId, String email, String first_name, String last_name, String image, String userId, String accessToken, int type) {
                if (!isConnected(getApplicationContext())) ;
                else {
                    setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, latitide, longitude);
                }
            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(CustomerSignUpActivity.this, "Failed to authorize from facebook", Toast.LENGTH_SHORT).show();
            }
        });
        facebookIntegrator.login();
    }


    void setSocialUser(final String social_id, final String username, final String fcm_token, final String email, final String first_name, final String last_name, final String image, Double lati, Double longi) {

//        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.SocialLogin(social_id, username, fcm_token, email, first_name, last_name, image, lati, longi);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            JSONObject profile = data.getJSONObject("userDetails");
                            String id = profile.getInt("id") + "";
                            userID = id;
                            String name = first_name + " " + last_name;
                            Constrants.user_name = name;
                            Constrants.Social_images = image;
                            editor.putString("Social_images", image).apply();
                            SaveSharedPreference.setUserId(getApplicationContext(), id);
                            SaveSharedPreference.setEmail(getApplicationContext(), email);

                            try {
                                phone1 = profile.getString("phone");
                                SaveSharedPreference.setPhone_Num(getApplicationContext(), phone1);
                                if (phone1.equals("null")) {
                                    if (!isConnected(getApplicationContext())) ;
                                    else {
                                        Intent intent = new Intent(CustomerSignUpActivity.this, PhoneAuthActivity.class);
                                        startActivityForResult(intent, 2);
                                    }
                                } else {
                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    finish();
                                }
                            } catch (Exception e) {
                                if (!isConnected(getApplicationContext())) ;
                                else {
                                    Intent intent = new Intent(CustomerSignUpActivity.this, PhoneAuthActivity.class);
                                    startActivityForResult(intent, 2);
                                    // Phone_Verification(id, fcm_token);
                                }
                            }


                        } else {
                            hideProgress();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(CustomerSignUpActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }

//    private void Phone_Verification(final String Social_id, final String fcm_token) {
//
//        integrator = new AccountKitIntegrator(this, new SocialCallback() {
//
//            @Override
//            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
//                Log.e("phonenumber", userId);
//                hideProgress();
//                if (!isConnected(getApplicationContext())) ;
//                else {
//                    phone_update(Social_id, fcm_token, userId);
//                }
//            }
//
//            @Override
//            public void onSocialLoginFailed(String error) {
//                hideProgress();
//                Toast.makeText(CustomerSignUpActivity.this, "Phone verification failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//        integrator.phoneLogin();
//    }

    void phone_update(String social_id, String fcm_token, String phone) {

//        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.Phone_Update(social_id, fcm_token, phone);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            hideProgress();
                            finish();

                        } else {
                            hideProgress();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(CustomerSignUpActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }

    public void doSignUp() {
        final String email;
        final String password;
        String confirmPass;
        String fullname;
        final String first_name;
        final String last_name;
        final String phone = "123654";


        email = edtEmail.getText().toString();
        password = edtPassword.getText().toString();
        confirmPass = edtConfirmPassword.getText().toString();
        fullname = edtFullName.getText().toString();
        final int random = new Random().nextInt(1000000);
        int firstSpace = fullname.indexOf(" ");
        if (firstSpace < 0) {
            wrapperFullName.setErrorEnabled(true);
            wrapperFullName.setError("Please Enter first name");
            return;
        } else {
            first_name = fullname.substring(0, firstSpace);
            last_name = fullname.substring(firstSpace).trim();
            if (last_name.equals("")) {
                wrapperFullName.setErrorEnabled(true);
                wrapperFullName.setError("Please Enter last name");
                return;
            } else if (first_name.equals(last_name)) {
                wrapperFullName.setErrorEnabled(true);
                wrapperFullName.setError("Please Correct your name");
                return;
            }
        }
        fullname = first_name + last_name + random;
        wrapperPassword.setErrorEnabled(false);
        wrapperEmail.setErrorEnabled(false);
        wrapperFullName.setErrorEnabled(false);
        wrapperConfirmPassword.setErrorEnabled(false);
        if (!Utils.isValidEmail(email)) {
            wrapperEmail.setErrorEnabled(true);
            wrapperEmail.setError("Invalid Email");
            return;
        }
        if (password.length() < 8) {
            wrapperPassword.setErrorEnabled(true);
            wrapperPassword.setError("Password length must be greater than 8");
            return;
        }
        if (!confirmPass.equals(password)) {
            wrapperConfirmPassword.setErrorEnabled(true);
            wrapperConfirmPassword.setError("Password does not match.");
            return;
        }
        if (fullname.isEmpty()) {
            wrapperFullName.setErrorEnabled(true);
            wrapperFullName.setError("Can't be empty");
            return;
        }

        if (!cbTerms.isChecked()) {
            Toast.makeText(this, "Please accept terms and policies", Toast.LENGTH_SHORT).show();
            return;
        }


         Fullname = fullname;
         Firstname = first_name;
         Lastname = last_name;
         Email = email;
         Password=password;

        Intent intent = new Intent(CustomerSignUpActivity.this, PhoneAuthActivity.class);
        startActivityForResult(intent, 3);
//        integrator = new AccountKitIntegrator(this, new SocialCallback() {
//
//            @Override
//            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
//                Log.e("phonenumber", userId);
//                setUser(Fullname, Firstname, Lastname, Email, userId, radious, password, fcm_token);
//
//            }
//
//            @Override
//            public void onSocialLoginFailed(String error) {
//                Toast.makeText(CustomerSignUpActivity.this, "Phone verification failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//        integrator.phoneLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            Log.e("number", userID);
            phone_update(userID, fcm_token, data.getStringExtra("MESSAGE").toString());
            //do the things u wanted
        }else if (requestCode==3){
            setUser(Fullname,Firstname,Lastname,Email,data.getStringExtra("MESSAGE").toString(),radious,Password,fcm_token);
        }
//        if (integrator != null)
//            integrator.onActivityResult(requestCode, data);
        if (twitterLogin != null) twitterLogin.onActivityResult(requestCode, resultCode, data);
        if (this.callbackManager != null)
            this.callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    void setUser(String username, final String first_name, final String last_name, final String email, String phone, String radious, final String password, String fcm_token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = preferences.edit();
        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.createUser(username, first_name, last_name, email, phone, radious, password, fcm_token);

        //calling the api

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject error_msg;
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            JSONObject profile = data.getJSONObject("userDetails");
                            String id = profile.getInt("id") + "";
                            String name = first_name + " " + last_name;
                            String phone_num = profile.getString("phone");
                            SaveSharedPreference.setPhone_Num(getApplicationContext(),phone_num);
                            editor.putString("loginPassword", password).apply();
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                            Constrants.user_name = name;
                            SaveSharedPreference.setUserId(getApplicationContext(), id);
                            SaveSharedPreference.setEmail(getApplicationContext(), email);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        } else {
                            error_msg = json.getJSONObject("error_msg");
                            if (error_msg.has("phone") && !error_msg.isNull("phone") && error_msg.has("email") && !error_msg.isNull("email") && error_msg.has("fcm_token") && !error_msg.isNull("fcm_token")) {
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                JSONArray email = error_msg.getJSONArray("email");
                                JSONArray fcm_token = error_msg.getJSONArray("fcm_token");
                                String phone = phone_array.getString(0);
                                String _email = email.getString(0);
                                String _fcm_token = fcm_token.getString(0);
                                Toast.makeText(getApplicationContext(), phone + "\n" + _email + "\n" + _fcm_token, Toast.LENGTH_LONG).show();
                            } else if (error_msg.has("phone") && !error_msg.isNull("phone") && error_msg.has("email") && !error_msg.isNull("email")) {
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                JSONArray email = error_msg.getJSONArray("email");
                                String phone = phone_array.getString(0);
                                String _email = email.getString(0);
                                Toast.makeText(getApplicationContext(), phone + "\n" + _email, Toast.LENGTH_LONG).show();

                            } else if (error_msg.has("phone") && !error_msg.isNull("phone")) {
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                String phone = phone_array.getString(0);
                                Toast.makeText(getApplicationContext(), phone, Toast.LENGTH_LONG).show();
                            } else if (error_msg.has("email") && !error_msg.isNull("email")) {
                                JSONArray email = error_msg.getJSONArray("email");
                                String _email = email.getString(0);
                                Toast.makeText(getApplicationContext(), _email, Toast.LENGTH_LONG).show();
                            } else if (error_msg.has("fcm_token") && !error_msg.isNull("fcm_token")) {
                                JSONArray fcm_token = error_msg.getJSONArray("fcm_token");
                                String _fcm_token = fcm_token.getString(0);
                                Toast.makeText(getApplicationContext(), _fcm_token, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


}
