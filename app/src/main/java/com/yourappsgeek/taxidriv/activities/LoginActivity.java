package com.yourappsgeek.taxidriv.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.FacebookSdk;
import com.facebook.login.widget.DeviceLoginButton;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.interfaces.PhoneNumberCallBack;
import com.yourappsgeek.taxidriv.socialIntegrator.social.AccountKitIntegrator;
import com.yourappsgeek.taxidriv.socialIntegrator.social.FacebookIntegrator;
import com.yourappsgeek.taxidriv.socialIntegrator.social.SocialCallback;
import com.yourappsgeek.taxidriv.socialIntegrator.social.TwitterIntegrator;
import com.yourappsgeek.taxidriv.utils.UiUtils;
import com.yourappsgeek.taxidriv.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.delight.android.location.SimpleLocation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;

public class LoginActivity extends AppCompatActivity {

    FacebookIntegrator facebookIntegrator;
    AccountKitIntegrator integrator;
    CallbackManager callbackManager;
    @BindView(R.id.fblogin)
    LoginButton fblogin;
    @BindView(R.id.edtEmail)
    EditText editEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;


    @BindView(R.id.wrapperEmail)
    TextInputLayout wrapperEmail;
    @BindView(R.id.wrapperPassword)
    TextInputLayout wrapperPassword;

    TwitterIntegrator twitterIntegrator;

    @BindView(R.id.twitterLogin)
    TwitterLoginButton twitterLogin;


    ProgressDialog progressDialog;
    private String token;

    @OnClick(R.id.btnSignIn)
    void signInClick() {
        if (!isConnected(getApplicationContext())) ;
        else {
            userSignIn();
        }
    }


    @OnClick(R.id.txtDontHaveAccount)
    void signUpClick() {
        startActivity(new Intent(LoginActivity.this, CustomerSignUpActivity.class));
        finish();
    }

    @OnClick(R.id.btnFacebook)
    void signInFb() {
        if (!isConnected(getApplicationContext())) ;
        else {
            doFbLogin();
        }
    }

    @OnClick(R.id.btnTwitter)
    void signInTwitter() {
        if (!isConnected(getApplicationContext())) ;
        else {
            doTwitterLogin();
        }
    }

    String fcm_token;
    String userID;
    final String radious = "3";
    String phone1;
    private final String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION};
    double latitude, logitude;
    private static final int REQUEST_CODE = 12345;
    SharedPreferences preference;
    SharedPreferences.Editor editor;
    private SimpleLocation location;
    TextView forgot;
    TextView skip_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);
        FacebookSdk.fullyInitialize();
        FacebookSdk.sdkInitialize(this);
        Twitter.initialize(this);


        location = new SimpleLocation(this);
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }

        try {
            if (Build.VERSION.SDK_INT >= 28) {
                @SuppressLint("WrongConstant") final PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNING_CERTIFICATES);
                final Signature[] signatures = packageInfo.signingInfo.getApkContentsSigners();
                final MessageDigest md = MessageDigest.getInstance("SHA");
                for (Signature signature : signatures) {
                    md.update(signature.toByteArray());
                    final String signatureBase64 = new String(Base64.encode(md.digest(), Base64.DEFAULT));
                    Log.d("SignatureBase64", signatureBase64);
                }
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        forgot = findViewById(R.id.txtForgotPassword);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotActivity.class));
                finish();
            }
        });

        latitude = location.getLatitude();
        logitude = location.getLongitude();
        progressDialog = UiUtils.getProgressDialog(this);
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[1]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[2]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[3]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, permissions, REQUEST_CODE);
        }


        getFCMTocken();
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        preference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = preference.edit();

        skip_login = findViewById(R.id.skip);
        Skip();


        Log.e("location", latitude + " " + logitude + "");

    }


    public void Skip() {
        skip_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    public void doTwitterLogin() {
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        twitterIntegrator = new TwitterIntegrator(twitterLogin, new SocialCallback() {

            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {

                try {
                    if (latitude != 0 && logitude != 0 && fcm_token != null) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            progressDialog.show();
                            setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, latitude, logitude);
                            Log.d("Response", "email" + email + " " + "fcm_token" + fcm_token + " " + latitude);
                        }
                    } else {
                        Log.d("Response", "email" + email + " " + "fcm_token" + fcm_token + " " + latitude);

                        Toast.makeText(LoginActivity.this, "Location Not Found!", Toast.LENGTH_SHORT).show();

                    }
                    hideProgress();
//                    setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, "31.4351714", "74.4262213");
                } catch (Exception e) {
                    hideProgress();
                    Toast.makeText(LoginActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSocialLoginFailed(String error) {
                hideProgress();
                Toast.makeText(LoginActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
            }
        });
        twitterIntegrator.login();
    }


    public void doFbLogin() {
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        facebookIntegrator = new FacebookIntegrator(fblogin, callbackManager, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String SocialId, String email, String first_name, String last_name, String image, String userId, String accessToken, int type) {

                try {
                    if (latitude != 0 && logitude != 0 && fcm_token != null) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            progressDialog.show();
                            setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, latitude, logitude);
                            //  Log.d("Response", "email" + email + " " + "fcm_token" + fcm_token + " " + latitude,);
                        }
                    } else {

                        Toast.makeText(LoginActivity.this, "Location Not Found!", Toast.LENGTH_SHORT).show();

                    }
                    hideProgress();
//                    setSocialUser(SocialId, username, fcm_token, email, first_name, last_name, image, "31.4351714", "74.4262213");
                } catch (Exception e) {
                    hideProgress();
                    Toast.makeText(LoginActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
        facebookIntegrator.login();
    }


    void setSocialUser(final String social_id, final String username, final String fcm_token, final String email, final String first_name, final String last_name, final String image, Double lati, Double longi) {


//        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.SocialLogin(social_id, username, fcm_token, email, first_name, last_name, image, lati, longi);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            JSONObject profile = data.getJSONObject("userDetails");
                            String id = profile.getInt("id") + "";
                            String name = first_name + " " + last_name;
                            Constrants.user_name = name;
                            editor.putString("Social_images", image).apply();
                            SaveSharedPreference.setUserId(getApplicationContext(), id);
                            SaveSharedPreference.setEmail(getApplicationContext(), email);
                            userID = id;

                            try {
                                phone1 = profile.getString("phone");
                                SaveSharedPreference.setPhone_Num(getApplicationContext(), phone1);
                                if (phone1.equals("null")) {
                                    if (!isConnected(getApplicationContext())) ;
                                    else {
                                        Intent intent = new Intent(LoginActivity.this, PhoneAuthActivity.class);
                                        startActivityForResult(intent, 2);

                                    }
                                } else {
                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    finish();
                                }
                            } catch (Exception e) {
                                if (!isConnected(getApplicationContext())) ;
                                else {
                                    Intent intent = new Intent(LoginActivity.this, PhoneAuthActivity.class);
                                    startActivityForResult(intent, 2);
                                    //Phone_Verification(id, fcm_token);
                                }
                            }


                        } else {
                            hideProgress();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }


//    private void Phone_Verification(final String Social_id, final String fcm_token) {
//
//        integrator = new AccountKitIntegrator(this, new SocialCallback() {
//
//            @Override
//            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
//                Log.e("phonenumber", userId);
//
//                Log.e("userDetail", userId);
//                hideProgress();
//                if (!isConnected(getApplicationContext())) ;
//                else {
//                    phone_update(Social_id, fcm_token, userId);
//                }
//            }
//
//            @Override
//            public void onSocialLoginFailed(String error) {
//                hideProgress();
//                Toast.makeText(LoginActivity.this, "Phone verification failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//        integrator.phoneLogin();
//    }

    void phone_update(String user_id, String fcm_token, final String phone) {

//        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.Phone_Update(user_id, fcm_token, phone);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                            SaveSharedPreference.setPhone_Num(getApplicationContext(), phone);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            hideProgress();
                            finish();

                        } else {
                            hideProgress();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.show();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }

    private void userSignIn() {
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        final String email;
        final String password;
        email = editEmail.getText().toString();
        password = edtPassword.getText().toString();

        wrapperPassword.setErrorEnabled(false);
        wrapperEmail.setErrorEnabled(false);
        if (!Utils.isValidEmail(email)) {
            wrapperEmail.setErrorEnabled(true);
            wrapperEmail.setError("Invalid Email");
            return;
        }
        if (password.length() < 6) {
            wrapperPassword.setErrorEnabled(true);
            wrapperPassword.setError("Password length must be greater than 6");
            return;
        }


        progressDialog.show();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = preferences.edit();

        APIService service = APIUrl.getClient().create(APIService.class);


        Call<ResponseBody> call = service.userLogin(email, password, fcm_token, latitude, logitude);

        Log.d("Response", "email" + email + " " + "password" + password + " " + "fcm_token" + fcm_token + " " + latitude);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {

                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data = json.getJSONObject("data");
                            JSONObject userDetail = data.getJSONObject("userDetails");
                            String first_name = userDetail.getString("first_name");
                            String last_name = userDetail.getString("last_name");
                            String phone_num = userDetail.getString("phone");
                            editor.putString("loginPassword", password).apply();

                            String id = userDetail.getInt("id") + "";
                            String name = first_name + " " + last_name;
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                            Constrants.user_name = name;
                            if (userDetail.has("avatar") && !userDetail.isNull("avatar")) {
                                String img = userDetail.getString("avatar");
                                editor.putString("profile_image", img).apply();
                            }
                            SaveSharedPreference.setUserId(getApplicationContext(), id);
                            SaveSharedPreference.setPhone_Num(getApplicationContext(), phone_num);
                            SaveSharedPreference.setEmail(getApplicationContext(), email);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        } else {

                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }
                    } else {

                        Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


//    void hideProgress() {
//        if (progressDialog != null && progressDialog.isShowing())
//            progressDialog.dismiss();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            Log.e("number", userID);
            phone_update(userID, fcm_token, data.getStringExtra("MESSAGE").toString());
            //do the things u wanted
        }
//        if (integrator != null)
//            integrator.onActivityResult(requestCode, data);
        if (twitterLogin != null) twitterLogin.onActivityResult(requestCode, resultCode, data);
        if (this.callbackManager != null)
            this.callbackManager.onActivityResult(requestCode, resultCode, data);

    }


    private void getFCMTocken() {
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
                SaveSharedPreference.setFCM_Token(getApplicationContext(), token);
                Log.e("FB_token ", token);
                // send it to server
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(LoginActivity.this, "Permission granted!!!", Toast.LENGTH_LONG).show();

                //locationn();

            } else {
                Toast.makeText(LoginActivity.this, "Necessary permissions not granted...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        location.beginUpdates();
    }


    @Override
    protected void onPause() {
        // stop location updates (saves battery)
        location.endUpdates();

        // ...

        super.onPause();
    }


    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


}
