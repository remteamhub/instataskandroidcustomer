package com.yourappsgeek.taxidriv.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.yourappsgeek.taxidriv.Connection.networkConnection;
import com.yourappsgeek.taxidriv.Controller.DialogController;
import com.yourappsgeek.taxidriv.CircleImageView;
import com.yourappsgeek.taxidriv.InternetConnection.InternetConnection;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.Singleton.SocketInstance;
import com.yourappsgeek.taxidriv.models.Payment_model;
import com.yourappsgeek.taxidriv.response.ParseJson;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.yourappsgeek.taxidriv.Controller.internetCheck.isConnected;
import static com.yourappsgeek.taxidriv.Controller.internetCheck.isGPSEnabled;
import static com.yourappsgeek.taxidriv.Service.Constrants.Social_images;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane1;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane2;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane3;
import static com.yourappsgeek.taxidriv.Service.Constrants.check;
import static com.yourappsgeek.taxidriv.Service.Constrants.customer_approvel;
import static com.yourappsgeek.taxidriv.Service.Constrants.edit_location_address;
import static com.yourappsgeek.taxidriv.Service.Constrants.jobID;
import static com.yourappsgeek.taxidriv.Service.Constrants.latitude;
import static com.yourappsgeek.taxidriv.Service.Constrants.longitude;
import static com.yourappsgeek.taxidriv.Service.Constrants.servicePrice;
import static com.yourappsgeek.taxidriv.Service.Constrants.status;
import static com.yourappsgeek.taxidriv.Service.Constrants.title;
import static com.yourappsgeek.taxidriv.Service.Constrants.userId;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, AdapterView.OnItemSelectedListener {
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 200;

    Button button;
    View headerview;
    float percentage;
    EditText enter_code;
    boolean locate = false;
    boolean spiner = false;
    double lati, longi;
    private GoogleMap mMap;
    Spinner payment_method;
    TextView header_name, spinner_text, rate_value, leave_rate, deliver_rate, confirm_rate;
    String spinner_possition;
    String token, result;
    List<Place.Field> fields;
    ArrayMap<Integer, LatLng> latLngHashMap;
    ArrayMap<Integer, LatLng> latLngHashMapSocket;
    private IntentFilter mIntent;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    ArrayAdapter<String> dataAdapter;
    List<Payment_model> payment_models;
    MaterialRatingBar materialRatingBar, header_rating, leave_rating, deliver_rating, confirm_rating;
    AppLocationService appLocationService;
    private BroadcastReceiver statusReceiver;
    boolean doubleBackToExitPressedOnce = false;
    private static final float DEFAULT_ZOOM = 20.0f;
    private static final String TAG = "MapActivity";
    String apiKey = "AIzaSyDRThsOQmQXD3F7FpoUnTkAWJtFY3gKCNc";
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    // The entry points to the Places API.
    private Location mLastKnownLocation;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private CameraPosition mCameraPosition;
    ProgressBar before, after;
    ImageButton imageButton;
    String provider_image = "null";
    private LocationManager locationManager;
    private android.location.LocationListener myLocationListener;

    //Select lines .........
    ImageView close;
    JSONArray data;
    // boolean check = false;
    TextView locationAddress;
    RelativeLayout layout, topaddress;
    // CheckBox cbLane1, cbLane2, cbLane3;
    //..................................

    //Confirm Service BS........
    RelativeLayout confirmDialog;
    TextView price, lines, userName, locationAddres, card;
    String id, addresss;
    //..................................

    //Deliver task bottom sheet........
    Button approve, disapprove;
    NestedScrollView scroll;
    RelativeLayout scrollView;
    ConstraintLayout scroll_constraint;
    RoundedImageView before_start_work, after_work_image;
    TextView accept_job, time, find_provider, locationAdres, provider_name, name_provider;
    //...............................

    //Rating Bar......................
    ConstraintLayout ratingbar;
    TextView provider_nam, pay_method, rate_price;
    private boolean mLocationPermissionGranted;
    private int timmerValue;
    //................................


    //Request for edit job....................
    RelativeLayout editjobrequest;

    CircleImageView header_img, confirm_layout_img, provider_profile_img, ratig_image, deliver_layout_image;

    String body, api, ntitle;
    DialogController controller;
    networkConnection connection;
    private String user_img;
    RelativeLayout editjob;
    String providerName = null;
    Button messages;
    Marker marker, socketMarker;
    Socket mSocket;
    SocketInstance instance;




    @Override
    protected void onDestroy() {
        super.onDestroy();
        connection.unregisterNetworkChanges(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }


        connection = new networkConnection(getApplicationContext());
        connection.registerNetworkBroadcastForNougat(this);

        setContentView(R.layout.activity_main);
        instance = (SocketInstance) getApplication();
        mSocket = instance.getSocketInstance().connect();
        if (mSocket.connected())
        Toast.makeText(getApplicationContext(), mSocket.connect().toString(), Toast.LENGTH_SHORT).show();
        try {
            userId = Integer.parseInt(SaveSharedPreference.getUser_ID(getApplicationContext()));

        } catch (Exception e) {
            userId = 0;
            e.printStackTrace();
        }

        materialRatingBar = findViewById(R.id.ratingBar5);

        latLngHashMap = new ArrayMap<>();
        latLngHashMapSocket = new ArrayMap<>();
        Constrants.progressDialog = UiUtils.getProgressDialog(this);
        token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        Constrants.FCM_Token = token;
        payment_method = findViewById(R.id.tvPaymentSelect);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = preferences.edit();
        payment_method.setOnItemSelectedListener(this);
        provider_nam = findViewById(R.id.textView32);
        pay_method = findViewById(R.id.textView23);
        rate_price = findViewById(R.id.rate_price);
        scroll_constraint = findViewById(R.id.scroll_constraint);
        enter_code = findViewById(R.id.editText);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerview = navigationView.getHeaderView(0);
        header_img = headerview.findViewById(R.id.imageView);
        header_rating = headerview.findViewById(R.id.header_rating);
        rate_value = headerview.findViewById(R.id.hd_value);
        imageButton = findViewById(R.id.edit_button);
        leave_rating = findViewById(R.id.RatingBar);
        deliver_rating = findViewById(R.id.ratingBar4);
        confirm_rating = findViewById(R.id.RatingBar1);
        leave_rate = findViewById(R.id.textView34);
        deliver_rate = findViewById(R.id.deliver_rate);
        confirm_rate = findViewById(R.id.confirm_rate);
        before = findViewById(R.id.before_p);
        after = findViewById(R.id.after_p);
        controller = new DialogController(this);
        controller.EditButton(this);
        confirm_layout_img = findViewById(R.id.circleImageView2);
        provider_profile_img = findViewById(R.id.circleImageView3);
        ratig_image = findViewById(R.id.circleImageView5);
        deliver_layout_image = findViewById(R.id.circleImageView);
        editjob = findViewById(R.id.editjob_layout);
        messages = findViewById(R.id.button4);
        messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ChatActivity.class));
            }
        });


//        destination_location=Constrants.dialog.findViewById(R.id.textView52);
//        destination_location.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, Constrants.fields)
//                        .build(getApplicationContext());
//                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
//            }
//        });


        try {
            body = Constrants.body;
            api = Constrants.api;

            Log.e("notificarionData", body + " " + api);

            if (body != null && api != null) {

                if (api.contains("Job_Edit_Enable")) {
                    controller.showDialog();
                } else if (api.contains("Job_Edit_Cancel")) {
                    controller.showEditJobDialog();
                } else {
                    if (!isConnected(getApplicationContext())) ;
                    else {
                        check_current_job(String.valueOf(userId), "0");
                    }
                }
            } else if (Constrants.mesg != null && Constrants.mesg.equals("You have new messages")) {
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        disapprove = findViewById(R.id.button6);
        disapprove.setClickable(false);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


//        limitation();

//        Select lines......................
        cbLane1 = findViewById(R.id.cbLane1);
        cbLane2 = findViewById(R.id.cbLane2);
        cbLane3 = findViewById(R.id.cbLane3);
        locationAddress = findViewById(R.id.location);
        cbLane1.setOnCheckedChangeListener(checkedChangeListener);
        cbLane2.setOnCheckedChangeListener(checkedChangeListener);
        cbLane3.setOnCheckedChangeListener(checkedChangeListener);
        layout = findViewById(R.id.rootxyz);
        topaddress = findViewById(R.id.topaddress);
        spinner_text = findViewById(R.id.spinner_text);
//..............................................


        //Deliver task bottom sheet........
        accept_job = findViewById(R.id.tv);
        time = findViewById(R.id.textView43);
        scrollView = findViewById(R.id.task);
        find_provider = findViewById(R.id.findprovider);
        approve = findViewById(R.id.btnApprove);
        approve.setEnabled(false);
        locationAdres = findViewById(R.id.textView35);
        provider_name = findViewById(R.id.textView33);
        name_provider = findViewById(R.id.service_provider);
        before_start_work = findViewById(R.id.imageView4);
        after_work_image = findViewById(R.id.imageView5);


//        Confirm Service BS....................
        final String name = Constrants.user_name;


        price = findViewById(R.id.textView30);
        lines = findViewById(R.id.textView27);
        userName = findViewById(R.id.textView24);
        card = findViewById(R.id.textView28);
        spinner_possition = SaveSharedPreference.getPayment_M(getApplicationContext());
        if (!spinner_possition.equals("")) {
            card.setText(spinner_possition);
        }
        locationAddres = findViewById(R.id.textView22);
        scroll = findViewById(R.id.scroll);

        confirmDialog = findViewById(R.id.confirmDialog);
        findViewById(R.id.button7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.INVISIBLE);
            }
        });
//...................................................................


//      Rating bar...................................
        ratingbar = findViewById(R.id.rate);

        // String img = SaveSharedPreference.getProfile_Image(getApplicationContext());
        try {
            Social_images = preferences.getString("Social_images", "");
            String pro_img = preferences.getString("profile_image", "");
            String img = APIUrl.IMAGE_BASE_URL + pro_img;
            if (!pro_img.equals("")) {
                Glide.with(getApplicationContext()).load(img).into(header_img);
                Glide.with(getApplicationContext()).load(img).into(confirm_layout_img);

            } else if (!Social_images.equals("")) {
                Glide.with(getApplicationContext()).load(Social_images).into(header_img);
            } else {
                Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(header_img);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        Places.initialize(getApplicationContext(), apiKey);
        preferences = getSharedPreferences("Lines", MODE_PRIVATE);

        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
        addresss = SaveSharedPreference.getLocation(getApplicationContext());

        if (addresss != null) {
            locationAddres.setText(addresss);
            locationAddress.setText(addresss);
            locationAdres.setText(addresss);
        }


//        String title1 = SaveSharedPreference.getService_Lane(getApplicationContext());
//        price1 = SaveSharedPreference.getPrice(getApplicationContext());


        spinner_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Payment_Method.class));
                finish();
            }
        });


        enter_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 0 || event.getAction() == KeyEvent.ACTION_DOWN) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                    Constrants.progressDialog.show();
                    String code = enter_code.getText().toString();
                    if (!isConnected(getApplicationContext())) ;
                    else {
                        validateCoupons(code);
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });


        appLocationService = new AppLocationService(
                MainActivity.this);
        findViewById(R.id.tvOrderTo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddPlaceButtonClicked(v);


            }
        });

        findViewById(R.id.imgGo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isConnected(getApplicationContext())) ;
                else {
                    try {
//            if (mLocationPermissionsGranted) {

                        final Task location = mFusedLocationProviderClient.getLastLocation();
                        location.addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "onComplete: found location!");
                                    Location currentLocation = (Location) task.getResult();
                                    if (currentLocation != null && InternetConnection.checkConnection(getApplicationContext())) {

                                        lati = currentLocation.getLatitude();
                                        longi = currentLocation.getLongitude();

                                        getJobLocation(lati, longi);


                                    } else {

                                        Toast.makeText(getApplicationContext(), "Please enable location or internet!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {

                                    Log.d(TAG, "onComplete: current location is null");
                                    Toast.makeText(MainActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                                }
                                hideProgress();
                            }
                        });
//            }
                    } catch (SecurityException e) {
                        Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
                    }
                }


            }
        });

        findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinner_possition = SaveSharedPreference.getPayment_M(getApplicationContext());
                String spinner_possitio = spinner_text.getText().toString();
                if (check) {
                    //  showBottomSheetDialogFragment();

                    if (spinner_possitio.equals("")) {
                        card.setText(spinner_possition);
                        layout.setVisibility(View.INVISIBLE);
                        confirmDialog.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Select Card!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Select Line!", Toast.LENGTH_LONG).show();
                }
            }
        });


        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isConnected(getApplicationContext())) ;
                else {
//                    job_id = Integer.parseInt(preferences.getString("job_id", "0"));
                    if (jobID != 0) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            float getrating = materialRatingBar.getRating();
                            customer_approve(jobID, 1, getrating);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Job id not found!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        disapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isConnected(getApplicationContext())) ;
                else {
//                    job_id = Integer.parseInt(preferences.getString("job_id", "0"));
                    if (jobID != 0) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            customer_approve(jobID, 0, 0);
                        }
                    }
                }
            }
        });


        button = findViewById(R.id.btn_go);
        Log.d("checkbutton", button.getText().toString());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (button.getText().equals("GO")) {
                    // price1 = SaveSharedPreference.getPrice(getApplicationContext());
                    if (lati != 0 && longi != 0) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            jobcreate(title, servicePrice, lati, longi, addresss, String.valueOf(userId));
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "location not found!", Toast.LENGTH_SHORT).show();
                    }
                } else if (button.getText().equals("CANCEL")) {
//                    job_id = Integer.parseInt(preferences.getString("job_id", "0"));
                    if (jobID != 0) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            cancle_job(jobID);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Job Id not found!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        pay_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Payment_Method.class));
                finish();
            }
        });
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isConnected(getApplicationContext())) ;
                else {
                    scroll.setVisibility(View.INVISIBLE);
                    ratingbar.setVisibility(View.VISIBLE);
                    if (!provider_image.equals("null")) {
                        String img = APIUrl.IMAGE_BASE_URL + provider_image;
                        Glide.with(getApplicationContext()).load(img).into(ratig_image);
                    }
                    rate_price.setText("$" + servicePrice);

                    provider_nam.setText(providerName);
                }
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        header_name = headerview.findViewById(R.id.header_name);
        if (userId == 0) {
            header_name.setText("Demo User");
        } else
            header_name.setText(name);
        checkGPSStatus();

        Spinner();
        if (!mSocket.connected()) {
            instance.getSocketInstance().connect();
        } else
            setListening();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        checkLocation();
        getLocationPermission();
        // Turn on the My Location layer and the related control on the map.
        // updateLocationUI();

        // Get the current location of the device and set the position of the map.
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));

        } else if (id == R.id.nav_my_request) {
            startActivity(new Intent(MainActivity.this, RequestActivity.class));

        } else if (id == R.id.nav_payment) {
            startActivity(new Intent(MainActivity.this, Payment_Method.class));
            finish();

        } else if (id == R.id.nav_help) {
            startActivity(new Intent(MainActivity.this, HelpActivity.class));
        } else if (id == R.id.nav_logout) {
            if (!isConnected(getApplicationContext())) ;
            else {
                logout();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void logout() {

        final APIService request = APIUrl.getClient().create(APIService.class);

        Call<ResponseBody> call = request.userLogOut(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                            SaveSharedPreference.setFCM_Token(getApplicationContext(),"");
                            SaveSharedPreference.setUserId(getApplicationContext(),"0");
                            SaveSharedPreference.setPhone_Num(getApplicationContext(), "null");

                            editor.putString("profile_image", "").apply();
                            editor.putString("Social_images", "").apply();

                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                            finish();
                        } else if (code == 405) {
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                            SaveSharedPreference.setFCM_Token(getApplicationContext(),"");
                            SaveSharedPreference.setUserId(getApplicationContext(),"0");
                            editor.putString("profile_image", "").apply();
                            editor.putString("Social_images", "").apply();
                            SaveSharedPreference.setPhone_Num(getApplicationContext(), "null");
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }


    public void onAddPlaceButtonClicked(View view) {
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK && InternetConnection.checkConnection(getApplicationContext())) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                LatLng latLng = place.getLatLng();
                lati = latLng.latitude;
                longi = latLng.longitude;
//        // Add a marker in Sydney and move the camera
                if (marker != null) {
                    marker.remove();
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
                getJobLocation(lati, longi);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                //  Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    public void service() {

        Constrants.progressDialog.show();
        APIService request = APIUrl.getClient().create(APIService.class);

        Call<ResponseBody> call = request.services(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONArray && code == 200) {
                            data = json.getJSONArray("data");
                            layout.setVisibility(View.VISIBLE);


                        } else {

                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }


    private void getJobLocation(double latitude, double longitude) {

//            if (mLocationPermissionsGranted) {

        try {
            moveCamera(new LatLng(latitude, longitude),
                    DEFAULT_ZOOM);

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
                sb.append(address.getAddressLine(0));
                result = sb.toString();

                if (Constrants.edit_location == 0) {

                    if (lati != 0 && longi != 0) {
                        if (!isConnected(getApplicationContext())) ;
                        else {
                            near_by_driver(lati, longi);
                        }
                        topaddress.setVisibility(View.VISIBLE);
                        SaveSharedPreference.setLocation(getApplicationContext(), result);
                        addresss = SaveSharedPreference.getLocation(getApplicationContext());
                        //  Log.e("Unable", addresss);
                        locationAddres.setText(addresss);
                        locationAddress.setText(addresss);
                        locationAdres.setText(addresss);
//                                        lat = String.valueOf(lati);
//                                        lng = String.valueOf(longi);
                        service();
                    }
                } else {
                    Constrants.latitude = latitude;
                    Constrants.longitude = longitude;

                    edit_location_address.setText(sb.toString());

                    Constrants.edit_location = 1;

                }
            } else {
                Toast.makeText(MainActivity.this, "Location not Found!", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e) {
            Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        JSONObject object = new JSONObject();
        try {
            object.put("lat", lati);
            object.put("lng", longi);
            mSocket.emit("getDriversData", object, new Ack() {
                @Override
                public void call(Object... args) {
                    JSONObject position = (JSONObject) args[0];
                    Log.i("callback", String.valueOf(position));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        near_by_driver(lati, longi);
    }


    private void getLocationPermission() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            checkLocation();
        }
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
//        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
//                android.Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            mLocationPermissionGranted = true;
//        } else {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
//                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, myLocationListener);
        }
//        switch (requestCode) {
//
//
//            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mLocationPermissionGranted = true;
//                    locate = true;
//                }
//
//            }
//
//        }

    }


//    private void updateLocationUI() {
//        if (mMap == null) {
//            return;
//        }
//        try {
//            if (mLocationPermissionGranted) {
//                mMap.setMyLocationEnabled(true);
//                mMap.getUiSettings().setMyLocationButtonEnabled(true);
//            } else {
//                mMap.setMyLocationEnabled(false);
//                mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                mLastKnownLocation = null;
//                getLocationPermission();
//            }
//        } catch (SecurityException e) {
//            Log.e("Exception: %s", e.getMessage());
//        }
//    }


    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            switch (compoundButton.getId()) {
                case R.id.cbLane1: {
                    if (b) {
                        check = true;
                        cbLane2.setChecked(false);
                        cbLane3.setChecked(false);
                        try {

                            JSONObject i = (JSONObject) data.get(0);
                            // object = data.getJSONObject(i);


                            Constrants.title = i.getString("title");
                            servicePrice = i.getString("price");
                            lines.setText(Constrants.title);
                            price.setText("$" + servicePrice);


//                            SaveSharedPreference.setService_Lane(getApplicationContext(), title);
//                            SaveSharedPreference.setPrice(getApplicationContext(), service_price);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    break;
                }
                case R.id.cbLane2: {

                    if (b) {
                        check = true;
                        cbLane1.setChecked(false);
                        cbLane3.setChecked(false);
                        try {
                            JSONObject i = (JSONObject) data.get(1);
                            Constrants.title = i.getString("title");
                            servicePrice = i.getString("price");
                            lines.setText(Constrants.title);
                            price.setText("$" + servicePrice);

//                            SaveSharedPreference.setService_Lane(getApplicationContext(), title);
//                            SaveSharedPreference.setPrice(getApplicationContext(), service_price);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    break;
                }
                case R.id.cbLane3: {
                    if (b) {
                        check = true;
                        cbLane2.setChecked(false);
                        cbLane1.setChecked(false);
                        JSONObject object = null;
                        try {
                            JSONObject i = (JSONObject) data.get(2);
                            // object = data.getJSONObject(i);
                            Constrants.title = i.getString("title");
                            servicePrice = i.getString("price");
                            lines.setText(Constrants.title);
                            price.setText("$" + servicePrice);

//                            SaveSharedPreference.setService_Lane(getApplicationContext(), title);
//                            SaveSharedPreference.setPrice(getApplicationContext(), service_price);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                }
            }

        }
    };


    public void checkLocation() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }


        myLocationListener = new android.location.LocationListener() {


            public void onLocationChanged(Location locationListener) {

                if (isGPSEnabled(getApplicationContext())) {
                    if (locationListener != null) {
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }

                        if (locationManager != null) {
                            mLastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (mLastKnownLocation != null) {
                                lati = mLastKnownLocation.getLatitude();
                                longi = mLastKnownLocation.getLongitude();
                                moveCamera(new LatLng(lati, longi), DEFAULT_ZOOM);
                            }
                        }
                    }
                } else if (isConnected(getApplicationContext())) {
                    if (locationManager != null) {
                        mLastKnownLocation = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (mLastKnownLocation != null) {
                            lati = mLastKnownLocation.getLatitude();
                            longi = mLastKnownLocation.getLongitude();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lati, longi), DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                }


            }

            public void onProviderDisabled(String provider) {

            }

            public void onProviderEnabled(String provider) {

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        };

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, myLocationListener);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String body = intent.getStringExtra("BODY");
            String api = intent.getStringExtra("API");
            final String action = intent.getAction();

            if (body != null && api != null) {
                Log.d("Status", "receive");

                if (api.contains("Job_Edit_Enable")) {
                    controller.showDialog();
                } else if (api.contains("Job_Edit_Cancel")) {
                    controller.showEditJobDialog();
                } else {
                    if (!isConnected(getApplicationContext())) ;
                    else {
                        check_current_job(String.valueOf(userId), "0");
                    }
                }
//                limitation();
                Constrants.api = null;
                Constrants.body = null;
            }
        }
    };

    void jobcreate(String title, final String service_price, double lat, double lng, final String location_address, final String customer_id) {


        try {
            Constrants.progressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.jobcreate(title, service_price, lat, lng, location_address, customer_id);

        //calling the api

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            token = SaveSharedPreference.getFCM_Token(getApplicationContext());
                            if (lati != 0.0 && longi != 0.0) {
                                latitude = lati;
                                longitude = longi;
                                ParseJson.update_latlng(MainActivity.this, lati, longi, Constrants.FCM_Token);
                            }
                            JSONObject data = json.getJSONObject("data");
                            button.setText("CANCEL");
//                            new CountDownTimer(30000, timmerValue) {
//
//                                public void onTick(long millisUntilFinished) {
//                                    Log.d("timmer", String.valueOf(millisUntilFinished));
//                                }
//
//                                public void onFinish() {
//                                    button.setText("GO");
//                                    button.setEnabled(false);
//                                }
//                            }.start();
//                            editor.putString("job_id", data.getInt("last_insert_id") + "").commit();

                            jobID = data.getInt("last_insert_id");
                            find_provider.setVisibility(View.VISIBLE);
//                        JSONObject data = json.getJSONObject("data");


                        }
                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    void check_current_job(final String user_id, final String type) {

        hideProgress();
        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.check_current_job(user_id, type);

        //calling the api
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        token = SaveSharedPreference.getFCM_Token(getApplicationContext());
                        if (lati != 0.0 && longi != 0.0) {
                            ParseJson.update_latlng(MainActivity.this, lati, longi, Constrants.FCM_Token);
                        }
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");

                        double user_rating;
                        double provider_rating = 0;
                        JSONObject job = null;
                        JSONObject provider = null;
                        JSONObject user = null;

                        String token = null;

                        if (json.get("data") instanceof JSONObject && code == 200) {


                            JSONObject data = json.getJSONObject("data");
                            if (data.has("job")) {
                                job = data.getJSONObject("job");
                                status = job.getInt("job_status");
                                user = job.getJSONObject("user");
                                user_rating = user.getDouble("rating");
                                token = user.getString("fcm_token");
                                jobID = job.getInt("id");
                                title = job.getString("service_name");
                                servicePrice = job.getString("service_price");
                                Constrants.User_ID = job.getString("customer_id");
                                Constrants.Provider_ID = job.getString("provider_id");
                                customer_approvel = job.getString("customer_approval");

                                if (job.has("provider") && !job.isNull("provider")) {
                                    provider = job.getJSONObject("provider");
                                    provider_rating = provider.getDouble("rating");
                                    providerName = provider.getString("first_name") + " " + provider.getString("last_name");
                                    provider_name.setText(providerName);
                                    if (provider.has("avatar") && !provider.isNull("avatar")) {
                                        provider_image = provider.getString("avatar");
                                        if (!provider_image.equals("null")) {
                                            String img = APIUrl.IMAGE_BASE_URL + provider_image;
                                            Glide.with(MainActivity.this).load(img).into(deliver_layout_image);
                                            Glide.with(MainActivity.this).load(img).into(ratig_image);
                                            Glide.with(getApplicationContext()).load(img).into(provider_profile_img);

                                        } else {
                                            provider_image = "null";
                                            Glide.with(MainActivity.this).load(R.drawable.ic_profile_placeholder).into(deliver_layout_image);
                                            Glide.with(MainActivity.this).load(R.drawable.ic_profile_placeholder).into(ratig_image);
                                            Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(provider_profile_img);
                                        }
                                    }


                                }


                                price.setText("$" + servicePrice);
                                lines.setText(title);
                                Constrants.user_name = user.getString("first_name") + " " + user.getString("last_name");
                                Constrants.profile_image = user.getString("avatar");
                                String img = APIUrl.IMAGE_BASE_URL + Constrants.profile_image;
                                header_name.setText(Constrants.user_name);

                                if (user.has("avatar") && !user.isNull("avatar")) {
                                    String user_im = user.getString("avatar");
                                    user_img = APIUrl.IMAGE_BASE_URL + user_im;

                                }


                                if (!Constrants.profile_image.contains("null")) {

                                    Glide.with(getApplicationContext()).load(img).into(header_img);
                                    Glide.with(getApplicationContext()).load(img).into(confirm_layout_img);
                                } else if (!Social_images.equals("")) {
                                    Glide.with(getApplicationContext()).load(Constrants.Social_images).into(header_img);
                                    Glide.with(getApplicationContext()).load(Social_images).into(confirm_layout_img);

                                } else {
                                    Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(header_img);
                                }
                                userName.setText(Constrants.user_name);

                            } else {
                                status = data.getInt("job_status");
                                user = data.getJSONObject("user");
                                user_rating = user.getDouble("rating");
                                token = user.getString("fcm_token");

                                Constrants.user_name = user.getString("first_name") + " " + user.getString("last_name");
                                Constrants.profile_image = user.getString("avatar");
                                header_name.setText(Constrants.user_name);

                                String img = APIUrl.IMAGE_BASE_URL + Constrants.profile_image;
                                if (!Constrants.profile_image.contains("null")) {
                                    Glide.with(getApplicationContext()).load(img).into(header_img);
                                } else if (!Social_images.equals("")) {
                                    Glide.with(getApplicationContext()).load(Constrants.Social_images).into(header_img);
                                } else {
                                    Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(header_img);
                                }
                                userName.setText(Constrants.user_name);
                            }


                            if (data.has("user") && !data.isNull("user")) {

                                user = data.getJSONObject("user");

                                if (user.has("fcm_token") && user.isNull("fcm_token")) {
                                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                                    finish();
                                } else if (user.has("fcm_token") && !user.isNull("fcm_token")) {
                                    String token1 = user.getString("fcm_token");
                                    if (!token.equals(token1)) {
                                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                        SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                                        finish();
                                    }
                                }

                            }

                            if (status == 5) {


                                String img = APIUrl.IMAGE_BASE_URL + job.getString("current_situation_img");

                                if (job.getString("current_situation_img") != null) {
                                    before.setVisibility(View.GONE);
                                    Glide.with(getApplicationContext()).load(img).into(before_start_work);
                                } else {
                                    before.setVisibility(View.VISIBLE);
                                    Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(before_start_work);
                                }

                            } else if (status == 8) {

                                String img = APIUrl.IMAGE_BASE_URL + job.getString("current_situation_img");
                                String imge = APIUrl.IMAGE_BASE_URL + job.getString("after_work_img");

                                if (job.getString("after_work_img") != null && job.getString("current_situation_img") != null) {
                                    Glide.with(getApplicationContext()).load(img).into(before_start_work);
                                    Glide.with(getApplicationContext()).load(imge).into(after_work_image);
                                    if (img != null && imge != null) {
                                        before.setVisibility(View.GONE);
                                        after.setVisibility(View.GONE);
                                    } else {
                                        before.setVisibility(View.VISIBLE);
                                        after.setVisibility(View.VISIBLE);
                                    }
                                    if (imge != null) {
                                        approve.setEnabled(true);
                                        disapprove.setEnabled(true);
                                    }

                                } else {
                                    Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(before_start_work);
                                    Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(after_work_image);
                                    before.setVisibility(View.VISIBLE);
                                    after.setVisibility(View.VISIBLE);
                                }
                            }

                            if (user_rating != 0 || user_rating != 0.0) {
                                DecimalFormat df = new DecimalFormat("0.00");
                                float user_rate = Float.parseFloat(String.format(df.format(user_rating)));
                                float pro_rate = Float.parseFloat(String.format(df.format(provider_rating)));
                                if (userId == 0)
                                    header_rating.setRating(5.0f);
                                else
                                    header_rating.setRating(user_rate);
                                leave_rating.setRating(pro_rate);
                                deliver_rating.setRating(pro_rate);
                                confirm_rating.setRating(user_rate);
                                rate_value.setText(user_rate + "/5.0");
                                leave_rate.setText(pro_rate + "/5.0");
                                deliver_rate.setText(pro_rate + "/5.0");
                                confirm_rate.setText(user_rate + "/5.0");
                                editor.putString("rate_value", user_rate + " ").commit();

                                Log.e("rate", user_rate + "");
                            }


                            switch (status) {

                                case 1: //leave for job
                                    time.setVisibility(View.VISIBLE);
                                    accept_job.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.VISIBLE);
                                    accept_job.setText("Service provider is on the way!");
                                    confirmDialog.setVisibility(View.GONE);
                                    find_provider.setVisibility(View.GONE);
                                    scroll.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    imageButton.setVisibility(View.VISIBLE);
                                    break;

                                case 2://pending
                                    //button.setEnabled(false);
                                    button.setText("CANCEL");
                                    find_provider.setVisibility(View.VISIBLE);
                                    confirmDialog.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.GONE);
                                    scroll.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    imageButton.setVisibility(View.GONE);
                                    editjob.setVisibility(View.GONE);


                                    break;

                                case 3://cancle
                                    button.setText("GO");
                                    scrollView.setVisibility(View.GONE);
                                    confirmDialog.setVisibility(View.GONE);
                                    accept_job.setVisibility(View.GONE);
                                    find_provider.setVisibility(View.GONE);
                                    scroll.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    editjob.setVisibility(View.GONE);
                                    if (!isConnected(getApplicationContext())) ;
                                    else {
                                        near_by_driver(lati, longi);
                                    }
                                    imageButton.setVisibility(View.GONE);

                                    break;

                                case 4://Accept
                                    button.setText("GO");
                                    time.setVisibility(View.GONE);
                                    scroll.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    confirmDialog.setVisibility(View.GONE);
                                    find_provider.setVisibility(View.GONE);
                                    accept_job.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.VISIBLE);
                                    imageButton.setVisibility(View.VISIBLE);
                                    accept_job.setText("Service provider accept the job!");
                                    break;

                                case 5: //working
                                    name_provider.setText(providerName);
                                    // setMargins(scroll_constraint, 0, 1050, 0, 0);
                                    if (!provider_image.equals("null")) {
                                        String img = APIUrl.IMAGE_BASE_URL + provider_image;
                                        Glide.with(MainActivity.this).load(img).into(deliver_layout_image);
                                    } else {
                                        Glide.with(MainActivity.this).load(R.drawable.ic_profile_placeholder).into(deliver_layout_image);

                                    }
                                    // Picasso.with(getApplicationContext()).load(provider_image).into(deliver_layout_image);
                                    scroll.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.GONE);
                                    accept_job.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    find_provider.setVisibility(View.GONE);
                                    confirmDialog.setVisibility(View.GONE);
                                    imageButton.setVisibility(View.GONE);
                                    editjob.setVisibility(View.GONE);

                                    break;

                                case 7: //arrived
                                    accept_job.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.VISIBLE);
                                    accept_job.setText("Service provider arrived on destination!");
                                    confirmDialog.setVisibility(View.GONE);
                                    find_provider.setVisibility(View.GONE);
                                    scroll.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    time.setVisibility(View.GONE);
                                    imageButton.setVisibility(View.GONE);
                                    editjob.setVisibility(View.GONE);

                                    break;

                                case 8: //request for approvel
                                    name_provider.setText(providerName);
                                    //setMargins(scroll_constraint, 0, 0, 0, 0);
                                    scroll.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.GONE);
                                    accept_job.setVisibility(View.GONE);
                                    ratingbar.setVisibility(View.GONE);
                                    imageButton.setVisibility(View.GONE);
                                    editjob.setVisibility(View.GONE);

                                    break;
                            }
                        } else {
                            Toast.makeText(MainActivity.this, json.getString("error_msg"), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });

    }

    void cancle_job(int job_id) {

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.cancle_job(job_id);

        //calling the api
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            button.setEnabled(true);
                            find_provider.setVisibility(View.INVISIBLE);
                            Toast.makeText(MainActivity.this, json.getString("success_msg"), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });

    }

    void near_by_driver(final double lat, double longi) {
        Log.e("Response", "enter");
        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.near_by_driver(lat, longi);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONArray && code == 200) {
                            JSONArray data = json.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                double lati = object.getDouble("lat");
                                double lngi = object.getDouble("lng");
                                int user_id = object.getInt("usr_id");

                                latLngHashMap.put(user_id, new LatLng(lati, lngi));
                                Log.e("Response", "Add data");
                                setListening();


//                                for (int j = 0; j < latLngHashMap.size(); j++) {
//                                    LatLng latLng = new LatLng(latLngHashMap.get(j).latitude, latLngHashMap.get(j)
//                                            .longitude);
//                                    Log.d("latlong", String.valueOf(latLng));
//                                    MarkerOptions marker = new MarkerOptions().position(latLng).title("Provider").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_snow_truck));
//                                    //  LatLng mylocation=new LatLng(lat,lngi);
//                                    //      mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mylocation, DEFAULT_ZOOM));
////                                    if (marker != null) {
////                                        mMap.clear();
////                                    }
//
//                                    mMap.addMarker(marker);
//
//                                }

                            }

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    void customer_approve(int job_id, int approve, float rating) {

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.customer_approve(job_id, approve, rating);

        //calling the api
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject object = json.getJSONObject("data");
                            String status = object.getString("status");
                            if (status.equals("approve")) {
                                ratingbar.setVisibility(View.INVISIBLE);
                                Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(before_start_work);
                                Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(after_work_image);
                            } else if (status.equals("disapprove")) {
                                scrollView.setVisibility(View.VISIBLE);
                                Glide.with(getApplicationContext()).load(R.drawable.no_uploaded).into(after_work_image);
                                after.setVisibility(View.VISIBLE);
                            }

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });

    }


//    public void limitation() {
//
//        APIService request = APIUrl.getClient().create(APIService.class);
//        Call<ResponseBody> call = request.limitations();
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    if (response.isSuccessful()) {
//                        JSONObject json = new JSONObject(response.body().string());
//                        Log.d("Response", json + "");
//                        int code = json.getInt("code");
//                        if (json.get("data") instanceof JSONObject && code == 200) {
//
//                            JSONObject data = json.getJSONObject("data");
//                            int second = data.getInt("max_job_acception_time");
//
//                            timmerValue = second * 1000;
//
//
//                        } else {
//                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
//                            hideProgress();
//                        }
//                    } else {
//                        Toast.makeText(MainActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
//                    }
//
//                } catch (JSONException e) {
//                    hideProgress();
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    hideProgress();
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                hideProgress();
//                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }

    void validateCoupons(String code) {
        APIService service = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = service.validateCoupons(code);
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        int code = json.getInt("code");

                        if (json.get("data") instanceof JSONObject && code == 200) {
                            //   price1 = SaveSharedPreference.getPrice(getApplicationContext());
                            JSONObject data = json.getJSONObject("data");
                            percentage = data.getInt("percentage");
                            float price2 = Float.parseFloat(servicePrice);
                            float value = (percentage * price2) / 100;
                            float discount_price = price2 - value;
                            int finalValue = Math.round(discount_price);

                            price.setText("$" + String.valueOf(finalValue));
                            Log.e("discount_price", String.valueOf(finalValue));
                            SaveSharedPreference.setPrice(getApplicationContext(), String.valueOf(finalValue));

                        } else {
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    private void checkGPSStatus() {
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setMessage("GPS not enabled");
            dialog.setCancelable(false);
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (locate) {
//            updateLocationUI();
//            locate = false;
//        }
        if (!isConnected(getApplicationContext())) ;
        else {
            check_current_job(String.valueOf(userId), "0");
        }
        Spinner();
        //registerReceiver(statusReceiver,mIntent);
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("NOW"));
        hideProgress();
    }

    @Override
    protected void onPause() {
        if (mIntent != null) {
            unregisterReceiver(statusReceiver);
            mIntent = null;
        }

        super.onPause();
    }


//    void hideProgress() {
//        if (progressDialog != null && progressDialog.isShowing())
//            progressDialog.dismiss();
//    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            super.onBackPressed();
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        SaveSharedPreference.setPayment_M(getApplicationContext(), parent.getItemAtPosition(position).toString());

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void Spinner() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = preferences.getString("paymentList", "");
        if (json.equals("[]") || json.equals("")) {
            spinner_text.setText("Select an payment method");
            spinner_text.setVisibility(View.VISIBLE);
            payment_method.setVisibility(View.GONE);
        } else {
            Type type = new TypeToken<List<Payment_model>>() {
            }.getType();
            List<Payment_model> arrPackageData = gson.fromJson(json, type);

            spiner = false;
            if (arrPackageData != null) {

                ArrayList<String> arrayList = new ArrayList<>();

                for (int i = 0; i < arrPackageData.size(); i++) {
                    Payment_model payment_model = arrPackageData.get(i);


                    arrayList.add("xxxx-xxxx-xxxx-" + payment_model.getCard_number());

                    Log.e("payment", String.valueOf(arrayList));
                }
                spinner_text.setText("");
                spinner_text.setVisibility(View.GONE);
                payment_method.setVisibility(View.VISIBLE);

                //  arrayList.add(0, "Select an payment method");
                dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                //  payment_method.setPrompt("Select an payment method");
                payment_method.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();
            }

        }

    }

    void hideProgress() {
        if (Constrants.progressDialog != null && Constrants.progressDialog.isShowing())
            Constrants.progressDialog.dismiss();
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }


    private void setListening() {
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.e("Response", "connected");

            }

        }).on("getDriversData", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.e("Response", "getDriversData");
                LatLng value;
                JSONArray array = (JSONArray) args[0];

                try {
                    JSONObject object = array.getJSONObject(0);
                    JSONArray resolve = object.getJSONArray("resolve");
                    for (int i = 0; i < resolve.length(); i++) {
                        JSONObject object1 = resolve.getJSONObject(i);
                        if (object1.has("lat") && !object1.isNull("lat") && object1.has("lng") && !object1.isNull("lng")) {
                            latitude = object1.getDouble("lat");
                            longitude = object1.getDouble("lng");
                            userId = object1.getInt("usr_id");
                            final LatLng socketData = new LatLng(latitude, longitude);
                            latLngHashMapSocket.put(userId, socketData);
                            Log.e("Response", String.valueOf(latLngHashMap.size()));
                            for (Object key : latLngHashMap.keySet()) {
                                value = latLngHashMap.get(key);
                                int a = Integer.valueOf(key.toString());
                                if (userId == a) {
                                    if (socketMarker != null) {
                                        socketMarker.remove();
                                    }
                                    value = socketData;
                                    final LatLng finalValue = value;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("Response", "show truck");
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(finalValue, DEFAULT_ZOOM));
                                            socketMarker = mMap.addMarker(new MarkerOptions().position(finalValue).title("Provider").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_snow_truck)));

                                        }
                                    });

                                    Log.e("data", "Key: " + key + " Value: " + finalValue);
                                }

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Driver not found", Toast.LENGTH_SHORT).show();
                        }
                    }


                } catch (Exception e) {
                    Log.e("Response", " " + e);
                }

            }

        }).on("status", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                //   JSONObject position = (JSONObject) args[0];
                Log.e("Response", "status");


            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                //  JSONObject position = (JSONObject) args[0];
                Log.e("Response", "disconnected");

            }


        });

    }

}
