package com.yourappsgeek.taxidriv.Controller;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;

import com.yourappsgeek.taxidriv.SharedPreferences.SaveSharedPreference;
import com.yourappsgeek.taxidriv.activities.LoginActivity;
import com.yourappsgeek.taxidriv.response.ParseJson;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.taxidriv.Service.Constrants.AUTOCOMPLETE_REQUEST_CODE;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane1;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane2;
import static com.yourappsgeek.taxidriv.Service.Constrants.cbLane3;
import static com.yourappsgeek.taxidriv.Service.Constrants.customerID;
import static com.yourappsgeek.taxidriv.Service.Constrants.customer_approvel;
import static com.yourappsgeek.taxidriv.Service.Constrants.data;
import static com.yourappsgeek.taxidriv.Service.Constrants.edit_location_address;
import static com.yourappsgeek.taxidriv.Service.Constrants.jobID;
import static com.yourappsgeek.taxidriv.Service.Constrants.latitude;
import static com.yourappsgeek.taxidriv.Service.Constrants.longitude;
import static com.yourappsgeek.taxidriv.Service.Constrants.servicePrice;
import static com.yourappsgeek.taxidriv.Service.Constrants.status;
import static com.yourappsgeek.taxidriv.Service.Constrants.title;


public class DialogController {
    ProgressDialog progressDialog;
    private Activity activity;
    RelativeLayout editjob;
    SharedPreferences preferences;

    public DialogController(Activity activity) {
        this.activity = activity;
        progressDialog = UiUtils.getProgressDialog(activity);
        preferences= PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
    }
    public void EditButton(final Activity activity){
        activity.findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                editjob=activity.findViewById(R.id.editjob);
//                editjob.bringToFront();
//                showDialog();

                ParseJson.editRequestStatus(activity,jobID);
            }
        });



    }


//    public static void Location(final Activity activity, String location){
//
//        if (location!=null){
//            destination_location.setText(location);
//        }
//    }



    public void showDialog(){
        Constrants.editbutton=true;
        Constrants.dialog = new Dialog(activity);
        Constrants.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Constrants.dialog.setCancelable(false);
        Constrants.dialog.setContentView(R.layout.edit_job);
        Window window = Constrants.dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
        checkButton(activity);
        edit_location_address=Constrants.dialog.findViewById(R.id.textView52);
        edit_location_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, Constrants.fields)
                        .build(activity);
                activity.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                Constrants.edit_location=1;
            }
        });

        Button dialogButton =Constrants.dialog.findViewById(R.id.confirmBtn);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location=edit_location_address.getText().toString();
                ParseJson.EditJob(activity,title,servicePrice,latitude,longitude,location,customerID, String.valueOf(jobID),customer_approvel,status);

            }
        });

        GetLocation controller=new GetLocation(activity);
        controller.Destination_location();

        final RelativeLayout editjob=activity.findViewById(R.id.editjob_layout);
        editjob.setVisibility(View.GONE);
        try {
            Constrants.dialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
        ParseJson.serviceApi(activity,Constrants.FCM_Token);
    }


    public void showEditJobDialog(){
        Constrants.dialog = new Dialog(activity);
        Constrants.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Constrants.dialog.setCancelable(false);
        Constrants.dialog.setContentView(R.layout.edit_job_request_dialog);
        Window window = Constrants.dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
        Button dialogButton =Constrants.dialog.findViewById(R.id.editjob_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constrants.dialog.dismiss();
               // ParseJson.EditJob(activity,title,servicePrice,latitude,longitude,Location_result,customerID,jobID,customer_approvel,status);

            }
        });

        final RelativeLayout editjob=activity.findViewById(R.id.editjob_layout);
        editjob.setVisibility(View.GONE);
        Constrants.dialog.show();

    }



    private void checkButton(Activity activity){
        cbLane1 = Constrants.dialog.findViewById(R.id.cbLane1);
        cbLane2 = Constrants.dialog.findViewById(R.id.cbLane2);
        cbLane3 = Constrants.dialog.findViewById(R.id.cbLane3);
        cbLane1.setOnCheckedChangeListener(checkedChangeListener);
        cbLane2.setOnCheckedChangeListener(checkedChangeListener);
        cbLane3.setOnCheckedChangeListener(checkedChangeListener);


    }

     CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            Log.e("click","click");
            switch (compoundButton.getId()) {
                case R.id.cbLane1: {
                    if (b) {
                        Constrants.check = true;
                        cbLane2.setChecked(false);
                        cbLane3.setChecked(false);

                        Log.e("click","click");
                            try {

                                JSONObject i = (JSONObject) Constrants.data.get(0);
                                // object = data.getJSONObject(i);

                                Log.e("title",i.getString("title"));
                                title = i.getString("title");
                                servicePrice=i.getString("price");



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                    }
                    break;
                }
                case R.id.cbLane2: {

                    if (b) {
                        Constrants.check = true;
                        cbLane1.setChecked(false);
                        cbLane3.setChecked(false);
                        Log.e("click","click");
                        JSONObject object = null;
                            try {

                                JSONObject i = (JSONObject) data.get(1);
                                title = i.getString("title");
                                servicePrice=i.getString("price");


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                    }
                    break;
                }
                case R.id.cbLane3: {
                    if (b) {
                        Constrants.check = true;
                        cbLane2.setChecked(false);
                        cbLane1.setChecked(false);
                        Log.e("click","click");
                        JSONObject object = null;
                            try {
                                JSONObject i = (JSONObject) data.get(2);
                                title = i.getString("title");
                                servicePrice=i.getString("price");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;
                }
            }

        }
    };



    public static void changepass_dialog(final Activity activity, final int id){
        final String fcm_token = SaveSharedPreference.getFCM_Token(activity.getApplicationContext());
        Constrants.dialog = new Dialog(activity);
        Constrants.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Constrants.dialog.setCancelable(false);
        Constrants.dialog.setContentView(R.layout.change_password);
        Window window = Constrants.dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
        Button button =Constrants.dialog.findViewById(R.id.new_submit);
        final AppCompatEditText new_pass=Constrants.dialog.findViewById(R.id.new_pass);
        final AppCompatEditText confirm_pass=Constrants.dialog.findViewById(R.id.confirm_pass);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(new_pass.getText().toString())) {
                    new_pass.setError("Enter New Password!");
                }else if (TextUtils.isEmpty(confirm_pass.getText().toString())){
                    confirm_pass.setError("Enter Confirm Password!");
                } else if (!new_pass.getText().toString().matches(confirm_pass.getText().toString())){
                    confirm_pass.setError("Password not match!");
                }else {
                    String c_pass=confirm_pass.getText().toString();
                    updatePassword(activity,id,fcm_token,"1",c_pass);
                }

            }
        });

        Constrants.dialog.show();
    }


    public static void updatePassword(final Activity activity, int id, String token, String old_password,String password) {
//        Gson gson = new GsonBuilder().setLenient().create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://instatask.trigoncab.com/api/provider/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
        APIService request = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = request.ChangePassword(id, token,old_password, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.putString("loginPassword", newP);
//                        editor.apply();
                        JSONObject data = json.getJSONObject("data");
                        Toast.makeText(activity, data.getString("status"), Toast.LENGTH_SHORT).show();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        activity.finish();
                        Constrants.dialog.dismiss();
                        hideProgress();

                    } else {
                        Toast.makeText(activity, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();

                } catch (IOException e) {
                    e.printStackTrace();
                    hideProgress();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }


    private static void hideProgress() {
        if (Constrants.progressDialog != null && Constrants.progressDialog.isShowing())
            Constrants.progressDialog.dismiss();
    }


}
