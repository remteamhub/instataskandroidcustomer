package com.yourappsgeek.taxidriv.Controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


import com.facebook.appevents.internal.Constants;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.taxidriv.models.HistoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.taxidriv.Service.Constrants.alertDialog;
import static java.util.Objects.isNull;

public class ScheduleRequest {

    String userId = String.valueOf(Constrants.userId);
    @SuppressLint("StaticFieldLeak")
    private Context context;
    ScheduleJobCallbackListener jobCallbackListener;

    public ScheduleRequest(Context context, ScheduleJobCallbackListener jobCallbackListener) {
        this.context = context;
        this.jobCallbackListener = jobCallbackListener;
    }


//    public void loadJobHistoryDetail(int type,int userID){
//
//        APIService request = APIUrl.getClient().create(APIService.class);
//        Call<ResponseBody> call = request.checkPendingJob(userId,"1");
//
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                if (response.isSuccessful()){
//                    try {
//                            JSONObject json=new JSONObject(response.body().string());
//                            Log.d("Response", json + "");
//                            int code = json.getInt("code");
//                            if (json.get("data") instanceof JSONObject && code==200){
//                                JSONObject data=json.getJSONObject("data");
//                                JSONArray job=data.getJSONArray("job");
//
//                                for (int i=0;i<job.length();i++){
//
//                                    JSONObject object=job.getJSONObject(i);
//                                    JSONObject user=object.getJSONObject("user");
//                                    HistoryItem historyItem=new HistoryItem.Schedule().
//                                            setJob_id(object.getInt("id")).setAddress(object.getString("location_address")).
//                                            setLatitude(object.getDouble("lat")).setLongitude(object.getDouble("lng")).
//                                            setPrice(object.getString("service_price")).setDate_time(user.getString("created_at")).
//                                            setCustomer_name(user.getString("first_name")+" "+user.get("last_name")).setService_type(object.getString("service_name")).setRating(user.getString("rating")).build();
//
//                                    jobCallbackListener.onFetchProgress(historyItem);
//
//                                }
//
//
//
//
//
//
//                            }else {
//                                Toast.makeText(context, json.getInt("msg"), Toast.LENGTH_SHORT).show();
//
//                            }
//                    jobCallbackListener.onFetchComplete();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }else {
//                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
//                jobCallbackListener.onFetchComplete();
//            }
//        });
//
//    }


    public void JobsHistory() {

        APIService request = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = request.checkPreviousJob(userId, "0");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONArray && code == 200) {
                            JSONArray data = json.getJSONArray("data");
//                            JSONArray job=data.getJSONArray("job");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject object = data.getJSONObject(i);


                                HistoryItem historyItem = null;

                                if (object.has("provider") && !object.isNull("provider") && object.has("user") && !object.isNull("user")) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        JSONObject user = object.getJSONObject("user");
                                        JSONObject provider = object.getJSONObject("provider");
                                        historyItem = new HistoryItem.Schedule().
                                                setJob_id(object.getInt("id")).setAddress(object.getString("location_address")).
                                                setLatitude(object.getDouble("lat")).setLongitude(object.getDouble("lng")).
                                                setPrice(object.getString("service_price")).setDate_time(object.getString("job_schedual_time")).
                                                setCustomer_name(provider.getString("first_name") + " " + provider.get("last_name")).setService_type(object.getString("service_name")).setRating(object.getString("driver_rating")).setBeforeImg(object.getString("current_situation_img")).setAfterImg(object.getString("after_work_img")).build();

                                        jobCallbackListener.onFetchProgress(historyItem);
                                    }
                                }



                            }


                        } else {
                            Toast.makeText(context, json.getInt("msg"), Toast.LENGTH_SHORT).show();

                        }
                        jobCallbackListener.onFetchComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();

                jobCallbackListener.onFetchComplete();
            }
        });

    }


    public static void updatePassword(final Context context, int id, String token, String oldP, final String newP) {
//        Gson gson = new GsonBuilder().setLenient().create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://instatask.trigoncab.com/api/provider/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
        final SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
//                .build();
        APIService request = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = request.updatePassword(id, token, oldP, newP);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("loginPassword", newP);
                        editor.apply();
                        JSONObject data = json.getJSONObject("data");
                        Toast.makeText(context, data.getString("status"), Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();

                    } else {
                        Toast.makeText(context, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                 //   hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
               // hideProgress();
            }
        });
    }


    public static void updateProfileImage(final Context context, int id, RequestBody token,RequestBody first_name,RequestBody last_name, MultipartBody.Part image) {
//        Gson gson = new GsonBuilder().setLenient().create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://instatask.trigoncab.com/api/provider/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
        final SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor=preferences.edit();
//                .build();
        APIService request = APIUrl.getClient().create(APIService.class);
        Call<ResponseBody> call = request.profileImage(id, token,first_name,last_name,image);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        JSONObject data = json.getJSONObject("data");
                        JSONObject userDetail=data.getJSONObject("userDetails");
                        Constrants.user_name=userDetail.getString("first_name")+" "+userDetail.getString("last_name");
                       // Constrants.profile_image=userDetail.getString("avatar");


                        if (userDetail.has("avatar") && !userDetail.isNull("avatar")) {
                            String img=userDetail.getString("avatar");
                            editor.putString("profile_image", img).apply();
                        }

                        Toast.makeText(context, "Update successfully!", Toast.LENGTH_LONG).show();


                       // Toast.makeText(context, data.getString("status"), Toast.LENGTH_SHORT).show();
                        //alertDialog.dismiss();

                    } else {
                        Toast.makeText(context, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                    //   hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                // hideProgress();
            }
        });
    }


}
