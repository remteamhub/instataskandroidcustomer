package com.yourappsgeek.taxidriv.interfaces;

import com.yourappsgeek.taxidriv.models.HistoryItem;

public interface ScheduleJobCallbackListener {

    void onFetchProgress(HistoryItem historyItem);
//    void onUpdateStatusProgress(UpdateStatus status);
    void onFetchComplete();
}
