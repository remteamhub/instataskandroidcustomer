package com.yourappsgeek.taxidriv.interfaces;

public interface PhoneNumberCallBack {
    void number(String number);
}
