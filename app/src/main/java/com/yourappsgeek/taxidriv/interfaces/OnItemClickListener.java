package com.yourappsgeek.taxidriv.interfaces;

import android.widget.CompoundButton;

import com.yourappsgeek.taxidriv.models.Payment_model;

public interface OnItemClickListener {
    void onItemClick(Payment_model item);
    void OnCheckClick(int possition, CompoundButton buttonView, boolean isChecked, String cardID);
}
