package com.yourappsgeek.taxidriv.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * This singleton class will make sure that each interactor operation gets a background thread
 *
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class ThreadExecutor
{
    private static final int CORE_POOL_SIZE = 3;
    private static final int MAX_POOL_SIZE = 5;
    private static final int KEEP_ALIVE_TIME = 120;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;
    private static final BlockingQueue<Runnable> WORK_QUEUE = new LinkedBlockingQueue<>();
    private static volatile ThreadExecutor threadExecutor;
    private ThreadPoolExecutor threadPoolExecutor;

    private ThreadExecutor()
    {
        this.threadPoolExecutor = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TIME_UNIT,
                WORK_QUEUE);
    }

    /**
     * Returns a singleton instance of this executor. If the executor is not initialized then it initializes
     * it and returns the instance
     *
     * @return {@link Executor} instance
     */
    public static ThreadExecutor getInstance()
    {
        if (threadExecutor == null)
        {
            threadExecutor = new ThreadExecutor();
        }

        return threadExecutor;
    }

    // @Override
    // public void execute(final BaseInteractor interactor)
    // {
    //     this.threadPoolExecutor.submit(() ->
    //     {
    //         // run the main logic
    //         interactor.run();
    //
    //         // mark it as finished
    //         interactor.onFinished();
    //     });
    // }
}
