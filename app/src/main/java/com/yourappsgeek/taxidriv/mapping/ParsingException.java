package com.yourappsgeek.taxidriv.mapping;


/**
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class ParsingException extends Exception
{
    private static final long serialVersionUID = 7876619284426341071L;

    public ParsingException()
    {
        super();
    }

    public ParsingException(final String message)
    {
        super(message);
    }

    public ParsingException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public ParsingException(final Throwable cause)
    {
        super(cause);
    }
}
