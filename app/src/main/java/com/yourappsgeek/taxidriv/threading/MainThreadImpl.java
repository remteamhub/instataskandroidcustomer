package com.yourappsgeek.taxidriv.threading;

import android.os.Handler;
import android.os.Looper;

import com.yourappsgeek.taxidriv.executor.MainThread;


/**
 * This class makes sure that the runnable we provide will be run on the main UI thread
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class MainThreadImpl implements MainThread
{
    private static MainThread mainThread;
    private Handler handler;

    private MainThreadImpl()
    {
        handler = new Handler(Looper.getMainLooper());
    }

    public static MainThread getInstance()
    {
        if (mainThread == null)
        {
            mainThread = new MainThreadImpl();
        }
        return mainThread;
    }

    @Override
    public void post(Runnable runnable)
    {
        handler.post(runnable);
    }
}
