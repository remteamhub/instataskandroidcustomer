package com.yourappsgeek.taxidriv.Notification;

import android.content.Intent;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
//        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//        Notification notification = new NotificationCompat.Builder(this)
//                .setContentTitle(remoteMessage.getData().get("title"))
//                .setContentText(remoteMessage.getData().get("body"))
//                .setLargeIcon(largeIcon)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .build();

        String body=remoteMessage.getData().get("body");
        String title=remoteMessage.getData().get("title");
        String api=remoteMessage.getData().get("api");
        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
     //   manager.notify(123, notification);
        Log.e("notificarionData",body + api);
        Intent in = new Intent();
        in.putExtra("BODY",body);
        in.putExtra("API",api);
        in.setAction("NOW");

        PushNotification.showNotification(getApplicationContext(),title,body);

        Log.e("notification", String.valueOf(in));
//sendBroadcast(in);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
    }
}