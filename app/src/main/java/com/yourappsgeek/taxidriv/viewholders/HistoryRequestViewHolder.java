package com.yourappsgeek.taxidriv.viewholders;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.activities.DetailsHistoryRequest;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 12/10/2018.
 */
public class HistoryRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    MapView mapView;

    public HistoryRequestViewHolder(@NonNull final View itemView)
    {
        super(itemView);
        mapView = itemView.findViewById(R.id.mapView2);
        itemView.setOnClickListener(this);


    }

    public void update(final Context context)
    {
        mapView.setOnClickListener(this);
        if (mapView != null) {
            // Initialise the MapView
            mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            mapView.getMapAsync(new OnMapReadyCallback()
            {
                @Override
                public void onMapReady(GoogleMap googleMap)
                {
                    // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                    googleMap.getUiSettings().setMapToolbarEnabled(false);

                    LatLng sydney = new LatLng(-34, 151);
                    googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));                    try
                    {
                        MapsInitializer.initialize(context);
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                //do nothing, we want to suppress launching Google Maps
                            }
                        });
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    @Override
    public void onClick(View view)
    {
        view.getContext().startActivity(new Intent(view.getContext(), DetailsHistoryRequest.class));

    }
}
