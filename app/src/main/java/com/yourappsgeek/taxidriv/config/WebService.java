package com.yourappsgeek.taxidriv.config;

/**
 * Contains relative paths of all the web-services
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public final class WebService
{
    public static final String LOGIN = "login/";
    public static final String REGISTER = "register/";
    public static final String LOGIN_CUSTOMER = LOGIN+"customer";
    public static final String REGISTER_CUSTOMER = REGISTER+"customer";
}
