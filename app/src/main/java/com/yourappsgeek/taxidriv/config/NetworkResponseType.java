package com.yourappsgeek.taxidriv.config;

/**
 * Contains all the supported response types
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public enum NetworkResponseType
{
    JSON,
    XML,
    SOAP
}
