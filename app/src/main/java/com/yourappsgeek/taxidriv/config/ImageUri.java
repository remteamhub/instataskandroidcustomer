package com.yourappsgeek.taxidriv.config;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUri {


    public static Uri getImageUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);


//        Uri uri=null;
////        try {
////        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
////        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
////
////        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
////        if (path!=null){
////            uri=Uri.parse(path);
////
////        }
////
////
////            bytes.close();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////
////        return uri;
    }

    public static String getRealPathFromURI(Context context,Uri uri) {
        if (uri == null) {
            return null;
        }
        Cursor cursor = null;
        String result = null;
        int column_index = 0;
        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, projection, null, null, null);

            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result=cursor.getString(column_index);
            }

        }catch (Exception e){

            e.printStackTrace();
        }finally {
            try {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

            } catch (Exception e) {
                Log.e("While closing cursor", String.valueOf(e));
            }
        }



        return result;
    }

}
