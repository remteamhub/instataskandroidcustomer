package com.yourappsgeek.taxidriv.Connection;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import com.yourappsgeek.taxidriv.receivers.NetworkChangeReceiver;
import com.yourappsgeek.taxidriv.utils.SnakeBar;


public  class networkConnection {
    private BroadcastReceiver  mNetworkReceiver ;
    public networkConnection(Context context) {
        mNetworkReceiver = new NetworkChangeReceiver();
    }




    public static void dialog(final Activity context, boolean value){

        if(value){
            Log.e("connection","back");
            Handler handler = new Handler();
            Runnable delayrunnable = new Runnable() {
                @Override
                public void run() {

                    SnakeBar.hideSnackbar();

                }
            };
            handler.postDelayed(delayrunnable, 3000);
        }else {
            Log.e("connection","gone");

            SnakeBar.snack(null, 0, "Network Connection failed.",context);

        }
    }


    public  void registerNetworkBroadcastForNougat(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            activity.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    public  void unregisterNetworkChanges(Activity activity) {

        try {
            activity.unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }



}
