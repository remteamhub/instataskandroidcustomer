package com.yourappsgeek.taxidriv.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yourappsgeek.taxidriv.models.Customer;

public class SignInResponse
{
    @SerializedName("_message")
    @Expose
    private String message;
    @SerializedName("customer")
    @Expose
    private Customer customer;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}


