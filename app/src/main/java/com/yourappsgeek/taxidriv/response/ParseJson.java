package com.yourappsgeek.taxidriv.response;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.Service.APIService;
import com.yourappsgeek.taxidriv.Service.APIUrl;
import com.yourappsgeek.taxidriv.Service.Constrants;
import com.yourappsgeek.taxidriv.activities.MainActivity;
import com.yourappsgeek.taxidriv.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParseJson {

    Activity activity;

    public ParseJson(Activity activity) {
        this.activity = activity;


    }

    public static void serviceApi(final Activity activity, String fcm_token) {

        Constrants.progressDialog = UiUtils.getProgressDialog(activity);
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = UiUtils.getProgressDialog(activity);
        final APIService request = APIUrl.getClient().create(APIService.class);

        Call<ResponseBody> call = request.services(fcm_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONArray && code == 200) {
                            Constrants.data = json.getJSONArray("data");

                            //  Constrants.dialog.dismiss();
                            //  hideProgress();


                        } else {
                            // hideProgress();
                            Toast.makeText(activity.getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(activity.getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

                hideProgress();


            }
        });

    }

    public static void EditJob(final Activity activity, String title, String service_price, final double lat, final double lng, String location_address, String customer_id, String job_id, String customer_approval, int job_status) {
        Constrants.progressDialog = UiUtils.getProgressDialog(activity);
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final APIService request = APIUrl.getClient().create(APIService.class);
        final ProgressDialog progressDialog = UiUtils.getProgressDialog(activity);
        Call<ResponseBody> call = request.EditJob(title, service_price, lat, lng, location_address, customer_id, job_id, customer_approval, job_status);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");

                        if (lat != 0.0 && lng != 0.0) {
                            ParseJson.update_latlng(activity, lat, lng, Constrants.FCM_Token);
                        }

                        Constrants.editbutton = false;

                        Constrants.dialog.dismiss();
                        Toast.makeText(activity.getApplicationContext(), json.getString("success_msg"), Toast.LENGTH_LONG).show();

                        //  hideProgress();
//                        } else {
//                            progressDialog.dismiss();
//                            Toast.makeText(activity.getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
//                        }

                    } else {

                        Toast.makeText(activity.getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();

            }
        });

    }


    public static void update_latlng(final Activity activity, double latitude, double longitude, String fcmToken) {
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.update_latlng(latitude, longitude, fcmToken);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                        }
                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

                hideProgress();


            }
        });

    }


    public static void editRequestStatus(final Activity activity, int job_id) {

        final RelativeLayout editjob;
        editjob = activity.findViewById(R.id.editjob_layout);
        Constrants.progressDialog = UiUtils.getProgressDialog(activity);
        try {
            Constrants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.editRequestStatus(job_id);

        //calling the api

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            editjob.setVisibility(View.VISIBLE);
                        }
                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();


            }
        });

    }


    public static void Help(final Activity activity, int customer_id, String user_type, String issue_name, String issue_type, String issue_dis, String status) {

        Constrants.progressDialog = UiUtils.getProgressDialog(activity);
        try{
            Constrants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        //Defining retrofit api service
        APIService service = APIUrl.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.help(customer_id, user_type, issue_name, issue_type, issue_dis, status);

        //calling the api

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            Toast.makeText(activity, json.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
              hideProgress();


            }
        });

    }

    private static void hideProgress() {

        if (Constrants.progressDialog != null && Constrants.progressDialog.isShowing())
            Constrants.progressDialog.dismiss();
    }

}
