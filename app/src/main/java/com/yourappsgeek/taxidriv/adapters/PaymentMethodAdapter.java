package com.yourappsgeek.taxidriv.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.interfaces.CallActivity;
import com.yourappsgeek.taxidriv.models.Payment_model;
import java.util.List;


public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentRequestHolder> {

    private Context context;
    private List<Payment_model> payment_methods;
    private CallActivity callActivity;
    private  OnItemClickListener listener;
    private int selectedPosition = 0;


    public PaymentMethodAdapter(Context context, List<Payment_model> payment_methods,CallActivity activity,OnItemClickListener listener) {
        this.callActivity=activity;
        this.context = context;
        this.payment_methods = payment_methods;
        this.listener=listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Payment_model item);
        void OnCheckClick(int possition,CompoundButton buttonView, boolean isChecked,Payment_model item);
    }
    @Override
    public PaymentMethodAdapter.PaymentRequestHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_method_items, viewGroup, false);
        PaymentRequestHolder holde = new PaymentRequestHolder(v);
        return holde;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final PaymentMethodAdapter.PaymentRequestHolder holder, final int possition) {


        holder.count.setText("Payment Detail "+payment_methods.get(possition).getCount());
       holder.date.setText(payment_methods.get(possition).getMonth()+"/"+payment_methods.get(possition).getYear());
       holder.card_number.setText("xxxx-xxxx-xxxx-"+payment_methods.get(possition).getCard_number());
       if (payment_methods.get(possition).getCard_type()!=null) {
           Picasso.with(context).load(payment_methods.get(possition).getCard_type()).into(holder.card_type);
       }
       else {
           Picasso.with(context).load(R.drawable.mastercard).into(holder.card_type);

       }

        if(possition == selectedPosition){
            holder.toggleButton.setChecked(true);
        } else {
            holder.toggleButton.setChecked(false);
        }

        holder.bind(payment_methods.get(possition), listener);
    }

    @Override
    public int getItemCount() {
        return payment_methods.size();
    }

    public class PaymentRequestHolder extends RecyclerView.ViewHolder {

        TextView date, card_number, count;
        ImageView card_type;
        public RelativeLayout viewBackground, viewForeground;
        ToggleButton toggleButton;

        public PaymentRequestHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.textView51);
            card_number = itemView.findViewById(R.id.textView50);
            card_type = itemView.findViewById(R.id.card_img);
            count = itemView.findViewById(R.id.textView48);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            toggleButton = itemView.findViewById(R.id.textView49);
        }

        public void bind(final Payment_model item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });


            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position=getAdapterPosition();
                    listener.OnCheckClick(position,buttonView,isChecked,item);
                }
            });
        }

    }



        public void removeItem(int position, String item) {
            payment_methods.remove(position);
            // notify the item removed by position
            // to perform recycler view delete animations
            // NOTE: don't call notifyDataSetChanged()
            callActivity.removeCard(item);
            notifyItemRemoved(position);
        }

        public void restoreItem(Payment_model item, int position) {
            payment_methods.add(position, item);
            // notify item added by position
            notifyItemInserted(position);
        }


}
