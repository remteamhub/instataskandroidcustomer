package com.yourappsgeek.taxidriv.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.fragments.RequestHistoryFragment;
import com.yourappsgeek.taxidriv.fragments.ScheduledRequestFragment;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 12/10/2018.
 */
public class ReqestPagerAdapter  extends FragmentPagerAdapter
{

    private Context mContext;

    public ReqestPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new ScheduledRequestFragment();
        } else {
            return new RequestHistoryFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.scheduled);
            case 1:
                return mContext.getString(R.string.history);
            default:
                return null;
        }
    }

}
