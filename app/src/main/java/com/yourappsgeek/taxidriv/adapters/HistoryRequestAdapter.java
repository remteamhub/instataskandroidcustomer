package com.yourappsgeek.taxidriv.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yourappsgeek.taxidriv.R;
import com.yourappsgeek.taxidriv.activities.DetailsHistoryRequest;
import com.yourappsgeek.taxidriv.interfaces.buttoncallback_listener;
import com.yourappsgeek.taxidriv.models.HistoryItem;

import java.text.SimpleDateFormat;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * @author Naveed Chaudhary
 * email :  naveedchaudhary300@gmail.com
 * Created on 30/07/2019.
 */
public class HistoryRequestAdapter extends RecyclerView.Adapter<HistoryRequestAdapter.ScheduleHolder>  {
    buttoncallback_listener buttoncallback_listener;
    List<HistoryItem> list;
    Context context;

    int layout;

    public HistoryRequestAdapter(List<HistoryItem> list, Context context, int layout,buttoncallback_listener listener)
    {
        this.context = context;
        this.list = list;
        this.layout = layout;
        this.buttoncallback_listener=listener;
    }

    public void addItem(HistoryItem schedule){
        list.add(schedule);
        notifyDataSetChanged();
    }




    @NonNull
    @Override
    public ScheduleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int possition) {
        View row = LayoutInflater.from(viewGroup.getContext()).inflate(layout, viewGroup, false);
        return new ScheduleHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleHolder scheduleHolder, final int possition) {



        try {
            final HistoryItem item=list.get(possition);
            scheduleHolder.address.setText(item.address);
            scheduleHolder.service_price.setText("$"+item.price);

            long unixdate = Long.parseLong(item.date_time);
            java.util.Date d = new java.util.Date(unixdate*1000L);
            String itemDateStr = new SimpleDateFormat("yyyy-MM-dd hh:mm aa").format(d);
            scheduleHolder.time_date.setText(itemDateStr);
            scheduleHolder.customer_name.setText(item.customer_name);
            scheduleHolder.service_type.setText(item.service_type);
            scheduleHolder.materialRatingBar.setRating(Float.parseFloat(item.rating));
        }catch (Exception e){
            e.printStackTrace();
        }
//        scheduleHolder.mapView.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//                googleMap.getUiSettings().setMapToolbarEnabled(false);
//                try
//                {
//                    MapsInitializer.initialize(context);
//                    LatLng sydney = new LatLng(item.latitude, item.longitude);
//                    googleMap.addMarker(new MarkerOptions().position(sydney).title("position"));
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                        @Override
//                        public void onMapClick(LatLng latLng) {
//                            //do nothing, we want to suppress launching Google Maps
//                        }
//                    });
//                } catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        });

        if (scheduleHolder.mapView != null) {
            // Initialise the MapView
            scheduleHolder.mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            scheduleHolder.mapView.getMapAsync(new OnMapReadyCallback()
            {
                @Override
                public void onMapReady(GoogleMap googleMap)
                {
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                    try
                    {
                        MapsInitializer.initialize(context);
                        LatLng sydney = new LatLng(list.get(possition).latitude, list.get(possition).longitude);
                        googleMap.addMarker(new MarkerOptions().position(sydney).title("You are here!"));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,15.5f));
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                //do nothing, we want to suppress launching Google Maps

                                Intent intent=new Intent(context, DetailsHistoryRequest.class);
                                intent.putExtra("lat",list.get(possition).latitude);
                                intent.putExtra("long",list.get(possition).longitude);
                                intent.putExtra("price",list.get(possition).price);
                                intent.putExtra("lane",list.get(possition).service_type);
                                intent.putExtra("provideName",list.get(possition).customer_name);
                                intent.putExtra("rating",list.get(possition).rating);
                                intent.putExtra("before_img",list.get(possition).before_img);
                                intent.putExtra("after_img",list.get(possition).after_img);
                                intent.putExtra("time",list.get(possition).date_time);
                                context.startActivity(intent);
                            }
                        });
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }

        scheduleHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                list.get(possition).address;

                Intent intent=new Intent(context, DetailsHistoryRequest.class);
                intent.putExtra("lat",list.get(possition).latitude);
                intent.putExtra("long",list.get(possition).longitude);
                intent.putExtra("price",list.get(possition).price);
                intent.putExtra("lane",list.get(possition).service_type);
                intent.putExtra("provideName",list.get(possition).customer_name);
                intent.putExtra("rating",list.get(possition).rating);
                intent.putExtra("before_img",list.get(possition).before_img);
                intent.putExtra("after_img",list.get(possition).after_img);




                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount()
    {
        return list.size();
    }




    public class ScheduleHolder extends RecyclerView.ViewHolder{


        TextView address,service_price,time_date,customer_name, service_type;
        MaterialRatingBar materialRatingBar;
        MapView mapView;
        Button button;
        public ScheduleHolder(@NonNull View itemView) {
            super(itemView);
            address=itemView.findViewById(R.id.textView12);
            service_price=itemView.findViewById(R.id.textView14);
            time_date=itemView.findViewById(R.id.textView13);
            customer_name=itemView.findViewById(R.id.textView40);
            service_type=itemView.findViewById(R.id.textView42);
            button=itemView.findViewById(R.id.button5);
            mapView=itemView.findViewById(R.id.mapView2);
            materialRatingBar=itemView.findViewById(R.id.ratingBar);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buttoncallback_listener.onClick(view,getAdapterPosition());
                }
            });
        }

    }


}