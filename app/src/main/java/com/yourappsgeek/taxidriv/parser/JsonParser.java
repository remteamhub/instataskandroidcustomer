package com.yourappsgeek.taxidriv.parser;

import com.yourappsgeek.taxidriv.response.RegisterResponse;
import com.yourappsgeek.taxidriv.response.SignInResponse;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/15/2018.
 */
public interface JsonParser
{
    SignInResponse parserSignIn(String data);
     RegisterResponse parserRegister(String data);

}
