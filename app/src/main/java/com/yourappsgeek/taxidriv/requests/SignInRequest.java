package com.yourappsgeek.taxidriv.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class SignInRequest extends WebServiceRequest
{

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fbAccessToken")
    @Expose
    private String fbAccessToken;
    @SerializedName("twAccessToken")
    @Expose
    private String twAccessToken;
    @SerializedName("twAccessTokenSecret")
    @Expose
    private String twAccessTokenSecret;

    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

    public String getTwAccessToken() {
        return twAccessToken;
    }

    public void setTwAccessToken(String twAccessToken) {
        this.twAccessToken = twAccessToken;
    }

    public String getTwAccessTokenSecret() {
        return twAccessTokenSecret;
    }

    public void setTwAccessTokenSecret(String twAccessTokenSecret) {
        this.twAccessTokenSecret = twAccessTokenSecret;
    }

}