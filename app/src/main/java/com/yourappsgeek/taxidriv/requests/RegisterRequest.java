package com.yourappsgeek.taxidriv.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/20/2018.
 */
public class RegisterRequest extends WebServiceRequest
{
    @SerializedName("fbAccessToken")
    @Expose
    private String fbAccessToken;
    @SerializedName("twAccessToken")
    @Expose
    private String twAccessToken;
    @SerializedName("twAccessTokenSecret")
    @Expose
    private String twAccessTokenSecret;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;

    public String getFbAccessToken()
    {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken)
    {
        this.fbAccessToken = fbAccessToken;
    }

    public String getTwAccessToken()
    {
        return twAccessToken;
    }

    public void setTwAccessToken(String twAccessToken)
    {
        this.twAccessToken = twAccessToken;
    }

    public String getTwAccessTokenSecret()
    {
        return twAccessTokenSecret;
    }

    public void setTwAccessTokenSecret(String twAccessTokenSecret)
    {
        this.twAccessTokenSecret = twAccessTokenSecret;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

}