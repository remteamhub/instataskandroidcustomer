package com.yourappsgeek.taxidriv.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/20/2018.
 */

public class StripeInfo {

    @SerializedName("stripeId")
    @Expose
    private String stripeId;

    public String getStripeId() {
        return stripeId;
    }

    public void setStripeId(String stripeId) {
        this.stripeId = stripeId;
    }

}