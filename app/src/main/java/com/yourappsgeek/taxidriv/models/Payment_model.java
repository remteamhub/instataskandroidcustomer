package com.yourappsgeek.taxidriv.models;

public class Payment_model {

    String month,year,count;
    String card_number;
    String card_type,id;
//
//    public Payment_model(String time, String date, int card_number, String card_type) {
//        this.time = time;
//        this.date = date;
//        this.card_number = card_number;
//        this.card_type = card_type;
//    }


    public Payment_model(String month, String year, String card_number, String count, String id) {
        this.month = month;
        this.year = year;
        this.count = count;
        this.card_number = card_number;
        this.card_type = card_type;
        this.id=id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }




    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }
}
