package com.yourappsgeek.taxidriv.models;

/**
 * @author Naveed Chaudhary
 * email : naveedchaudhary300@gmail.com
 * Created on 09/07/2019.
 */
public class HistoryItem
{
    public int job_id;
    public String address;
    public double latitude;
    public double longitude;
    public String price;
    public String date_time;
    public String customer_name;
    public String service_type;
    public String rating;
    public String before_img;
    public String after_img;


    public HistoryItem(Schedule schedule) {
        job_id=schedule.job_id;
        address=schedule.address;
        latitude=schedule.latitude;
        longitude=schedule.longitude;
        price=schedule.price;
        date_time=schedule.date_time;
        customer_name=schedule.customer_name;
        service_type=schedule.service_type;
        rating=schedule.rating;
        before_img=schedule.before_img;
        after_img=schedule.after_img;
    }


    public static class Schedule {
        int job_id;
        String address;
        double latitude;
        double longitude;
        String price;
        String date_time;
        String customer_name;
        String service_type;
        String rating;
        String before_img;
        String after_img;


        public Schedule setJob_id(int job_id) {
            this.job_id = job_id;
            return Schedule.this;
        }

        public Schedule setAddress(String address) {
            this.address = address;
            return Schedule.this;
        }

        public Schedule setLatitude(double latitude) {
            this.latitude = latitude; return Schedule.this;
        }

        public Schedule setLongitude(double longitude) {
            this.longitude = longitude; return Schedule.this;
        }

        public Schedule setPrice(String price) {
            this.price = price; return Schedule.this;
        }

        public Schedule setDate_time(String date_time) {
            this.date_time = date_time; return Schedule.this;
        }

        public Schedule setCustomer_name(String customer_name) {
            this.customer_name = customer_name; return Schedule.this;
        }

        public Schedule setService_type(String service_type) {
            this.service_type = service_type; return Schedule.this;
        }

        public Schedule setRating(String rating) {
            this.rating = rating;return Schedule.this;
        }

        public Schedule setBeforeImg(String before_img) {
            this.before_img = before_img;return Schedule.this;
        }

        public Schedule setAfterImg(String after_img) {
            this.after_img = after_img;return Schedule.this;
        }

        public HistoryItem build(){
            return new HistoryItem(Schedule.this);
        }

    }

}

