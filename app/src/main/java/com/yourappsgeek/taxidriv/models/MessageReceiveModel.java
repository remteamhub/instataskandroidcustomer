package com.yourappsgeek.taxidriv.models;

public class MessageReceiveModel {

    String receiveMesg,sendMesg,created_at,to,from,last_page;

    public MessageReceiveModel(String receiveMesg, String sendMesg, String created_at, String to, String from) {
        this.receiveMesg = receiveMesg;
        this.sendMesg = sendMesg;
        this.created_at = created_at;
        this.to = to;
        this.from = from;

    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getReceiveMesg() {
        return receiveMesg;
    }

    public void setReceiveMesg(String receiveMesg) {
        this.receiveMesg = receiveMesg;
    }

    public String getSendMesg() {
        return sendMesg;
    }

    public void setSendMesg(String sendMesg) {
        this.sendMesg = sendMesg;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLast_page() {
        return last_page;
    }

    public void setLast_page(String last_page) {
        this.last_page = last_page;
    }
}
