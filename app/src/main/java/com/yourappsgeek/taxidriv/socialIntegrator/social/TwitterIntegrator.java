package com.yourappsgeek.taxidriv.socialIntegrator.social;

import android.util.Log;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import retrofit2.Call;


public class TwitterIntegrator
{
    private TwitterLoginButton loginButton;
    private SocialCallback socialCallback;
    private TwitterAuthClient client;
    private String email,id,secret;
    private String userID;
    private String userName;
    private String imageProfileUrl;

    public TwitterIntegrator(TwitterLoginButton loginButton, SocialCallback socialCallback)
    {
        this.socialCallback = socialCallback;
        this.loginButton = loginButton;
        client = new TwitterAuthClient();
        RegisterCallback();
    }

    private void RegisterCallback()
    {
        loginButton.setCallback(new Callback<TwitterSession>()
        {
            @Override
            public void success(Result<TwitterSession> result)
            {
                try {
                    if (result.data!=null)
                    {
                        id = result.data.getAuthToken().token;
                        secret = result.data.getAuthToken().secret;
                        TwitterSession twitterSession = result.data;
                        fetchTwitterEmail(twitterSession);
                      //  fetchTwitterImage();



                        Log.e("Data",userName);



                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException exception)
            {
                try {
                    Log.e("exception", String.valueOf(exception));
                    Log.e("Data","faield");
                    socialCallback.onSocialLoginFailed("UserCancelled");
                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });
    }

    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                try {
                    email=result.data;
                    userID= String.valueOf(twitterSession.getUserId());
                    userName=twitterSession.getUserName();
                    fetchTwitterImage();
                    Log.e("Data",userName);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("Data","Failed to email authenticate. Please try again.");
            }
        });


    }




    public void fetchTwitterImage() {
        //check if user is already authenticated or not
        if (getTwitterSession() != null) {

            //fetch twitter image with other information if user is already authenticated

            //initialize twitter api client
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            //Link for Help : https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials

            //pass includeEmail : true if you want to fetch Email as well
            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    try {
                        User user = result.data;
                        imageProfileUrl = user.profileImageUrl;
                        String fullname = user.name;
                        String userSocialProfile = user.profileImageUrl;
                        String userEmail = user.email;
                        String userFirstNmae = fullname.substring(0, fullname.lastIndexOf(" "));
                        String userLastNmae = fullname.substring(fullname.lastIndexOf(" "));
                        String userScreenName = userFirstNmae+userLastNmae;
                        socialCallback.onSocialLoginSuccess(userName,userFirstNmae,userLastNmae,userEmail,userID,imageProfileUrl,null,id+"_"+secret,SocialCallback.TWITTER);

                        Log.e("Data",imageProfileUrl);
                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }

                @Override
                public void failure(TwitterException exception) {
                    Log.e("Data","Failed to image authenticate. Please try again.");
                }
            });
        } else {
            //if user is not authenticated first ask user to do authentication
           // Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }

    }

    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }

    public void login()
    {
        this.loginButton.performClick();
    }
}
