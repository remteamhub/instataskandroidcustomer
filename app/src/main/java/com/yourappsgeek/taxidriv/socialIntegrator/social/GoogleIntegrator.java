// package com.yourappsgeek.taxidriv.socialIntegrator.social;
//
// import android.content.Intent;
// import android.view.View;
//
// import com.google.android.gms.auth.api.signin.GoogleSignIn;
// import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
// import com.google.android.gms.auth.api.signin.GoogleSignInClient;
// import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
// import com.google.android.gms.common.SignInButton;
// import com.google.android.gms.common.api.ApiException;
// import com.google.android.gms.tasks.Task;
// import com.synavos.hospitall.R;
// import com.synavos.hospitall.base.BaseActivity;
// import com.synavos.hospitall.impl.login.SocialCallback;
//
// /**
//  * @author Furqan Ullah
//  * email : furqan.ullah@synavos.com
//  * Created on 4/4/2018.
//  */
//
// public class GoogleIntegrator
// {
//     SignInButton btnGoogleLogin;
//     SocialCallback socialCallback;
//     GoogleSignInOptions gso;
//     GoogleSignInClient mGoogleSignInClient;
//     BaseActivity context;
//     private int RC_SIGN_IN = 100;
//
//     public GoogleIntegrator(SignInButton btnGoogleLogin, View tvGoogleCustom, SocialCallback socialCallback, BaseActivity context)
//     {
//         this.btnGoogleLogin = btnGoogleLogin;
//         this.socialCallback = socialCallback;
//         this.context = context;
//         gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                 .requestEmail()
//                 .requestIdToken(context.getString(R.string.google_client_id))
//                 .build();
//         mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
//
//         this.btnGoogleLogin.setOnClickListener(view -> signIn());
//     }
//
//     public void signIn()
//     {
//         try
//         {
//
//             mGoogleSignInClient.signOut();
//         } catch (Exception e)
//         {
//             e.printStackTrace();
//         }
//         Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//         context.startActivityForResult(signInIntent, RC_SIGN_IN);
//     }
//
//     // TODO: This method name is wrong. Is it still working? (Umair)
//     public void onAcitvityResult(int requestCode, int resultCode, Intent data)
//     {
//         // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
//         if (requestCode == RC_SIGN_IN)
//         {
//             // The Task returned from this call is always completed, no need to attach
//             // a listener.
//             Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//             handleSignInResult(task);
//         }
//     }
//
//     private void handleSignInResult(Task<GoogleSignInAccount> completedTask)
//     {
//         try
//         {
//             GoogleSignInAccount account = completedTask.getResult(ApiException.class);
//             socialCallback.onSocialLoginSuccess(account.getId(), account.getIdToken(), SocialCallback.GOOGLE);
//
//         } catch (ApiException e)
//         {
//             socialCallback.onSocialLoginFailed("Failed: " + e.getLocalizedMessage());
//         }
//     }
// }
