package com.yourappsgeek.taxidriv.socialIntegrator.social;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 4/4/2018.
 */

public interface SocialCallback
{
    int FB = 0;
    int TWITTER = 1;
    int PHONE = 2;
//    String social_id=null;
//    String username =null;
//    String first_name = null;
//    String last_name=null;
//    String imageUrl=null;


    void onSocialLoginSuccess(String username, String first_name,String last_name,String email,String socialId,String image,String userId, String accessToken, int type);
    //void onSocialLoginSuccessful(String userId, String accessToken, int type, String email, String gender, String facebookName, String profilePictureView);

    void onSocialLoginFailed(String error);
}
