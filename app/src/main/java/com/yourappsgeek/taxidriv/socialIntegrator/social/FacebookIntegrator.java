package com.yourappsgeek.taxidriv.socialIntegrator.social;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.GraphRequest.TAG;


public class FacebookIntegrator {
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private SocialCallback socialCallback;
    private String social_id, username, email, first_name, last_name, imageUrl;
    String accessToken;
    Profile currentUserProfile;
    public FacebookIntegrator(LoginButton loginButton, CallbackManager callbackManager, SocialCallback socialCallback) {
        this.socialCallback = socialCallback;
        this.callbackManager = callbackManager;
        this.loginButton = loginButton;

        RegisterCallback();
    }

    private void RegisterCallback() {
        if(AccessToken.getCurrentAccessToken()!=null){

            GraphLoginRequest(AccessToken.getCurrentAccessToken());

        }else {
            GraphLoginRequest(AccessToken.getCurrentAccessToken());
        }

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            private ProfileTracker mProfileTracker;

            @Override
            public void onSuccess(final LoginResult loginResult) {
                accessToken = loginResult.getAccessToken().getToken();
                currentUserProfile = Profile.getCurrentProfile();
               // GraphLoginRequest(loginResult.getAccessToken());

                socialCallback.onSocialLoginSuccess(username, social_id, email, first_name, last_name, imageUrl, null,null, SocialCallback.FB);
            }

            @Override
            public void onCancel() {
                socialCallback.onSocialLoginFailed("UserCancelled");
            }

            @Override
            public void onError(FacebookException exception) {
                socialCallback.onSocialLoginFailed("Failed: " + exception.getLocalizedMessage());
            }
        });
    }

    protected void GraphLoginRequest(final AccessToken accessToken){
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {

                        if (graphResponse != null && graphResponse.getJSONObject() != null && graphResponse.getJSONObject().has("name")
                        )
                        {
                            try {

                             //   username = graphResponse.getJSONObject().getString("name");
                                first_name = graphResponse.getJSONObject().getString("first_name");
                                last_name = graphResponse.getJSONObject().getString("last_name");
                                social_id = graphResponse.getJSONObject().getString("id");
                                Log.e("onCompleted: ",social_id);
                                username=first_name+last_name+social_id;
                                email=graphResponse.getJSONObject().getString("email");
                                imageUrl=graphResponse.getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");

                            } catch (JSONException e) {
                               Log.e(TAG, "onCompleted: ",e );
                            }
                        }


                    }
                });

        Bundle bundle = new Bundle();
        bundle.putString(
                "fields",
                "id,name,link,email,gender,last_name,first_name,locale,timezone,updated_time,verified,picture.type(large)"
        );
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();

    }

    public void login() {
        LoginManager.getInstance().logOut();
        this.loginButton.performClick();
    }
}
